/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef PWMDDIALOG_H
#define PWMDDIALOG_H

#define VERSION "0.3"

#include <QDialog>
#include <QTreeWidget>
#include <QListWidget>
#include <QValidator>
#include "ui_pwmdDialog.h"
#include "pwmd.h"

class PwmdPrivate;
class PwmdDialog : public QDialog
{
    Q_OBJECT

    public:
	PwmdDialog(const QString &sock = 0, const QString &filename = 0,
		 const QString &name = "PwmdDialog", QWidget *parent = 0);
	~PwmdDialog();

	// Returns the element path of the selected item in the element tree
	// with each element separated by the specified character.
	QString elementPath(const QChar &sep = '\t');

	// Returns the opened data filename.
	QString filename();

	// Returns the path of the connected socket.
	QString socket();

	// Clears the contents of the copy-paste clipboard.
	static void clearClipboard();

    private slots:
	void slotElementSelected(QTreeWidgetItem *, QTreeWidgetItem *);
	void slotAttributeSelected(QListWidgetItem *, QListWidgetItem *);
	void slotFilenameSelected(QListWidgetItem *, QListWidgetItem *);
	void slotToggleShowContent(int);
	void slotRefreshElementTree();
	void slotRefreshFilenameTree();
	void slotSaveContent();
	void slotSaveDocument();
	void slotContentChanged();
	void slotFinished();
	void slotNewChildElement();
	void slotNewRootElement();
	void slotDeleteElement();
	void slotRenameElement();
	void slotEditAttributes();
	void slotConnectSocket();
	void slotOpenFile();
	void slotOpenNew();
	void slotEditElements();
	void slotNewAttribute();
	void slotDeleteAttribute();
	void slotOpenOk();
	void slotOpenCancel();
	void slotOpenNewOk();
	void slotOpenNewCancel();
	void slotNewAttributeOk();
	void slotNewAttributeCancel();
	void slotNewChildElementOk();
	void slotNewRootElementOk();
	void slotNewChildElementCancel();
	void slotEditOptions();
	void slotOptionsOk();
	void slotOptionsCancel();
	void slotJumpToTarget();
	void slotShowAll();
	void slotExpandAll();
	void slotLock();
	void slotAttributesOk();
	void slotShowPassword();
	void slotPasswordContentChanged(const QString &);
	void slotClearCacheEntry();
	void slotClipboardElementPath();
	void slotClipboardAttribute();
	void slotNewAttributeValueChanged();
	void slotChooseTargetColor();
	void slotChooseInvalidTargetColor();
	void slotChooseHilightColor();
	void slotClipboardPassword();
	void slotFilenameDoubleClicked(QListWidgetItem *);
	void slotReset();
	void slotEditLiteralAttributes();
	void slotEditTargetAttributes();
	void slotDndCopyElement();
	void slotDndMoveElement();
	void slotDndCreateTarget();
	void slotGeneratePassword();
	void slotGenerateTypeChanged(int);
	void slotExecuteCommandOk();
	void slotExecuteCommandCancel();
	void slotExecuteCommand();
	void slotInquireStateChanged(int);
	void slotExecuteContentChanged();
	void slotExpandElement();
	void slotCollapseElement();
	void slotRenameElementUpdate(QWidget *);
	void slotAboutQt();
	void slotSearchCommandResults();
	void slotCommandHistoryChanged(int);
	void slotElementEntered(QTreeWidgetItem *, int);
	void slotStackedWidgetRightChanged(int);
	void slotConnectionStateChanged(Pwmd::ConnectionState);
	void slotConnectionError(unsigned);
	void slotRemoteToggled(bool);
	void slotChooseIdentityFile();
	void slotChooseKnownhostsFile();
	void slotIpVersionToggled(bool);
	void slotClearClipboard();

    private:
	friend class PwmdTreeWidget;
	friend class PwmdListWidget;
	friend class PwmdPrivate;
	QString command(const QString &cmd, gpg_error_t &rc, bool verbose = true);
	static int statusCallback(void *, const char *);
#if LIBPWMD_VERSION >= 0x603
	static gpg_error_t knownhostCallback(void *data, const char *host,
		const char *key, size_t len);
#endif
	void setContent(const QString &);
	QString elementPath(const QTreeWidgetItem *item, const QChar &sep = '\t');
	QList<QTreeWidgetItem *>buildElementTree(const QString &path, gpg_error_t &rc);
	QList<QListWidgetItem *>buildAttributeList(QTreeWidgetItem *, gpg_error_t &);
	QList<QListWidgetItem *>buildFilenameTree(gpg_error_t &);
	gpg_error_t doSaveContent();
	void showError(gpg_error_t);
	bool verifyNewContent();
	void setBusy(bool b = true);
	void setConnected(bool = true);
	gpg_error_t saveDocument();
	bool doFinished(bool = true);
	gpg_error_t refreshElementTree(bool = false);
	gpg_error_t editElements();
	bool hasBadTarget(QTreeWidgetItem *, const QString &, gpg_error_t &);
	bool badTarget(const QTreeWidgetItem *);
	void setBadTarget(QTreeWidgetItem *, bool = true);
	QTreeWidgetItem *findElement(const QString &, bool = true);
	void setPasswordContent(const QString &);
	bool elementSelected(QTreeWidgetItem *);
	void refreshAttributeList(QTreeWidgetItem *);
	bool hasTarget(const QTreeWidgetItem *);
	QString elementName(const QTreeWidgetItem *, bool = false);
	void setItemText(QTreeWidgetItem *, const QString &);
	void tabify(QPlainTextEdit *);
	void paintEvent(QPaintEvent *);
	void setOpen(bool = true);
	void popupContextMenu();
	bool isParentOf(QTreeWidgetItem *, QTreeWidgetItem *);
	void setModified(bool = true);
	QString updateSocket();
	void closeEvent(QCloseEvent *);

	PwmdPrivate *pwm;
	Pwmd::ConnectionState state;
	QString _filename;
	QString sock;
	Ui::PwmdDialog ui;
	QTreeWidgetItem *selectedElement;
	QTreeWidgetItem *previousElement;
	QColor previousElementColor;
	QString selectedElementPath;
	QTreeWidgetItem *dropElement;
	QListWidgetItem *selectedAttribute;
	QString selectedFilename;
	unsigned iterationsOrig;
	bool remote;
	bool importRoot;
	QWidget *previousRightPage;
	QColor targetColor;
	QColor invalidTargetColor;
	QColor hilightColor;
	bool init;
	QString clientName;
	bool dndOperation;
	bool modifiedTarget;
	int ipVersion;
	QTimer *clipboardTimer;
};

class PwmdPasswordOnlyValidate : public QValidator
{
    public:
	State validate(QString &in, int &pos) const
	{
	    (void)pos;

	    for (int i = 0; i < in.length(); i++) {
		if (in.count(in.at(i)) > 1)
		    return Invalid;
	    }

	    return Acceptable;
	};
};

class PwmdPrivate : public Pwmd
{
    public:
	PwmdPrivate(const QString &s, const QString &f, const QString &n,
		PwmdDialog *p) : Pwmd(s, f, n)
	{
	    parent = p;
	    setLockOnOpen();
	    setErrorOnLocked();
	};

	QString command(const QString &cmd, gpg_error_t &rc,
		bool verbose = true, bool autoOpen = true)
	{
	    parent->setBusy();
	    QString s = Pwmd::command(cmd, rc, autoOpen);
	    parent->setBusy(false);

	    if (rc && rc != GPG_ERR_NO_VALUE) {
		if (verbose && rc != GPG_ERR_CANCELED)
		    parent->showError(rc);

		return s;
	    }

	    rc = 0;
	    return s;
	};

	gpg_error_t inquire(const char *cmd, const QString &data)
	{
	    parent->setBusy();
	    gpg_error_t rc = Pwmd::inquire(cmd, data);
	    parent->setBusy(false);

	    if (rc)
		parent->showError(rc);

	    return rc;
	};

	gpg_error_t connectSocket(bool doOpen = true)
	{
	    parent->setBusy();
	    Pwmd::setFilename(parent->filename());
	    gpg_error_t rc = Pwmd::connect(doOpen);
	    parent->setBusy(false);

	    if (rc)
		parent->showError(rc);

	    return rc;
	};

	gpg_error_t open()
	{
	    Pwmd::setFilename(parent->filename());

	    parent->setBusy();
	    gpg_error_t rc = Pwmd::open();
	    parent->setBusy(false);

	    if (rc) {
		if (rc != GPG_ERR_CANCELED)
		    parent->showError(rc);
	    }

	    return rc;
	};

	gpg_error_t save()
	{
	    parent->setBusy();
	    gpg_error_t rc = Pwmd::save();
	    parent->setBusy(false);

	    if (rc && rc != GPG_ERR_CANCELED)
		parent->showError(rc);
	    else if (!rc)
		parent->setModified(false);

	    return rc;
	};

    private:
	void setFilename(const QString &);

	PwmdDialog *parent;
};

#endif
