/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <QMenu>
#include "pwmdDialog.h"
#include "pwmdListWidget.h"

PwmdListWidget::PwmdListWidget(QWidget *p) : QListWidget(p)
{
    parent = NULL;
}

void PwmdListWidget::setParent(PwmdDialog *p)
{
    parent = p;
}

void PwmdListWidget::contextMenuEvent(QContextMenuEvent *ev)
{
    if (ev && ev->reason() == QContextMenuEvent::Other)
	return;

    if (parent->ui.stackedWidgetLeft->currentWidget() == parent->ui.openPage)
	return;

    parent->selectedAttribute = parent->ui.attributeList->currentItem();
    QMenu *m = new QMenu(this);
    QAction *a = m->addAction(tr("&New"), parent, SLOT(slotNewAttribute()));
    a = m->addAction(tr("&Delete"), parent, SLOT(slotDeleteAttribute()));

    if (!parent->selectedAttribute)
	a->setEnabled(false);

    a = m->addAction(tr("&Copy name"), parent, SLOT(slotClipboardAttribute()));

    if (!parent->selectedAttribute)
	a->setEnabled(false);

    m->exec(QCursor::pos());
    delete m;
}

void PwmdListWidget::keyPressEvent(QKeyEvent *ev)
{
    if (ev->key() == Qt::Key_Return && ev->nativeModifiers() == 8) {
	QCursor::setPos(mapToGlobal(QPoint(visualItemRect(currentItem()).topRight())));
	contextMenuEvent(NULL);
	return;
    }

    QListWidget::keyPressEvent(ev);
}
