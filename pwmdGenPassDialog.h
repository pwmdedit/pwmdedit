/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef PWMDGENPASSDIALOG_H
#define PWMDGENPASSDIALOG_H

#include <QDialog>
#include "ui_pwmdGenPassDialog.h"

class PwmdGenPassDialog : public QDialog
{
    Q_OBJECT

    public:
	PwmdGenPassDialog(QWidget *);
	~PwmdGenPassDialog() {};
	QString result();
	static QString generatePassword(QWidget *);
	static QString generate(const QString &, int);

	typedef enum {
	    Any, AlphaNumeric, Alpha, Numeric
	} Type;

	static QString generate(Type, int);

    private slots:
	void slotShowPassword();
	void slotGeneratePassword();
	void slotGenerateTypeChanged(int);
	void slotClipboardPassword();

    private:
	Ui::PwmdGenPassDialog ui;
	QString _result;
};

#endif
