/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include "pwmd.h"

void PwmdStatusMessageThread::run()
{
    for (;;) {
	if (parent->state() == Pwmd::Init)
	    break;

	parent->commandMutex->lock();
	gpg_error_t rc;
	pwmd_async_t s = pwmd_process(parent->pwm, &rc, NULL);
	parent->commandMutex->unlock();
	(void)s;

	if (rc) {
	    emit parent->connectionError(rc);
	    break;
	}

	msleep(100);
    }
}

Pwmd::Pwmd(const QString &sock, const QString &file, const QString &name) :
    statusMsgThread(this)
{
    _socket = sock;
    _filename = file;
    clientName = name;
    pwmd_init();
    statusCb = NULL;
    statusCbData = NULL;
    pwm = NULL;
    _lockOnOpen = false;
    _errorOnLocked = false;
    commandMutex = new QMutex(QMutex::Recursive);
    reset();
}

Pwmd::~Pwmd()
{
    resetHandle();
}

pwm_t *Pwmd::handle()
{
    return pwm;
}

void Pwmd::resetHandle()
{
    commandMutex->lock();
    _state = Init;

    if (pwm)
	pwmd_close(pwm);

    pwm = NULL;
    commandMutex->unlock();
    statusMsgThread.wait();
}

#if LIBPWMD_VERSION >= 0x603
gpg_error_t Pwmd::setKnownhostCallback(KnownhostCallback cb, void *data)
{
    gpg_error_t rc;

    rc = pwmd_setopt(pwm, PWMD_OPTION_KNOWNHOST_CB, cb);

    if (rc)
	return rc;

    knownhostCb = cb;
    rc = pwmd_setopt(pwm, PWMD_OPTION_KNOWNHOST_DATA, data);

    if (rc)
	return rc;

    knownhostCbData = data;
    return 0;
}
#endif

gpg_error_t Pwmd::reset()
{
    gpg_error_t rc;

    resetHandle();
    pwm = pwmd_new(clientName.isEmpty() ? NULL : clientName.toUtf8().data());
    rc = pwmd_setopt(pwm, PWMD_OPTION_STATUS_CB, statusCb);
    rc = pwmd_setopt(pwm, PWMD_OPTION_STATUS_DATA, statusCbData);
#if LIBPWMD_VERSION >= 0x603
    rc = pwmd_setopt(pwm, PWMD_OPTION_KNOWNHOST_CB, knownhostCb);
    rc = pwmd_setopt(pwm, PWMD_OPTION_KNOWNHOST_DATA, knownhostCbData);
#endif
    _state = Init;
    emit stateChanged(Init);
    return rc;
}

gpg_error_t Pwmd::setStatusCallback(StatusCallback cb, void *data)
{
    gpg_error_t rc;

    rc = pwmd_setopt(pwm, PWMD_OPTION_STATUS_CB, cb);

    if (rc)
	return rc;

    statusCb = cb;
    rc = pwmd_setopt(pwm, PWMD_OPTION_STATUS_DATA, data);

    if (rc)
	return rc;

    statusCbData = data;
    return 0;
}

QString Pwmd::command(const QString &cmd, gpg_error_t &rc, bool autoOpen)
{
    char *result = NULL;
    QString s = QString::null;

    if (state() != Opened) {
	rc = connect(autoOpen);

	if (rc)
	    goto fail;
    }

    commandMutex->lock();
    rc = pwmd_command(pwm, &result, "%s", cmd.toUtf8().data());
    commandMutex->unlock();

    if (result) {
	s = result;
	pwmd_free(result);
    }

fail:
    return s;
}

gpg_error_t Pwmd::inquireCallback(void *user, const char *cmd,
	gpg_error_t rc, char **data, size_t *len)
{
    if (rc)
	return rc;

    (void)cmd;
    QString *s = static_cast<QString *>(user);

    *data = s->toUtf8().data();
    *len = s->toUtf8().length();
    return GPG_ERR_EOF;
}

gpg_error_t Pwmd::inquire(const char *cmd, const QString &str)
{
    gpg_error_t rc = connect();

    if (!rc) {
	QString *data = new QString(str);

	commandMutex->lock();
	rc = pwmd_inquire(pwm, cmd, inquireCallback, data);
	commandMutex->unlock();
	delete data;
    }

    return rc;
}

gpg_error_t Pwmd::connect(bool doOpen)
{
    if (state() != Init) {
	if (doOpen && state() != Opened)
	    return open();

	return 0;
    }

    gpg_error_t rc = pwmd_connect_url(pwm, socket().toUtf8().data());

    if (!rc) {
	_state = Connected;
	statusMsgThread.start();
	emit stateChanged(Connected);

	if (doOpen && !filename().isEmpty())
	    rc = open();
    }
    else
	emit stateChanged(state());

    return rc;
}

void Pwmd::setLockOnOpen(bool b)
{
    _lockOnOpen = b;
}

bool Pwmd::lockOnOpen()
{
    return _lockOnOpen;
}

void Pwmd::setErrorOnLocked(bool b)
{
    _errorOnLocked = b;
}

bool Pwmd::errorOnLocked()
{
    return _errorOnLocked;
}

gpg_error_t Pwmd::open(bool force)
{
    if (state() == Opened && !force)
	return 0;

    pwmd_socket_t type;
    gpg_error_t rc = pwmd_socket_type(pwm, &type);
    commandMutex->lock();

    if (!rc) {
#if LIBPWMD_VERSION >= 0x603
	rc = pwmd_setopt(pwm, PWMD_OPTION_LOCK_ON_OPEN, lockOnOpen());
#else
	rc = pwmd_command(pwm, NULL, "SET LOCK_ON_OPEN=%i", lockOnOpen());
#endif

	if (!rc)
	    rc = pwmd_command(pwm, NULL, "SET RC_ON_LOCKED=%i", errorOnLocked());

	if (!rc) {
	    if (type == PWMD_SOCKET_LOCAL)
		rc = pwmd_open(pwm, filename().toUtf8());
	    else
		rc = pwmd_open2(pwm, filename().toUtf8());
	}

	_state = !rc ? Opened : Connected;
    }
    else
	_state = Init;

    commandMutex->unlock();
    emit stateChanged(state());
    return rc;
}

void Pwmd::setFilename(const QString &f)
{
    _filename = f;
}

QString Pwmd::filename()
{
    return _filename;
}

QString Pwmd::socket()
{
    return _socket;
}

void Pwmd::setSocket(const QString &s)
{
    _socket = s;
}

Pwmd::ConnectionState Pwmd::state()
{
    return _state;
}

gpg_error_t Pwmd::save()
{
    pwmd_socket_t type;
    gpg_error_t rc = pwmd_socket_type(pwm, &type);
    commandMutex->lock();

    if (!rc) {
	if (type == PWMD_SOCKET_LOCAL)
	    rc = pwmd_save(pwm);
	else
	    rc = pwmd_save2(pwm);
    }

    commandMutex->unlock();
    return rc;
}
