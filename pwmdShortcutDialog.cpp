/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <QSettings>
#include "pwmdDialog.h"
#include "pwmdShortcutDialog.h"

PwmdShortcutDialog::PwmdShortcutDialog(const QString &file,
	const QString &sock) : QDialog()
{
    filename = file;
    socket = sock;
    ui.setupUi(this);
    connect(ui.pb_newShortcut, SIGNAL(clicked()), SLOT(slotNewShortcut()));
    connect(ui.pb_removeShortcut, SIGNAL(clicked()), SLOT(slotRemoveShortcut()));
    connect(ui.lb_shortcuts, SIGNAL(currentItemChanged(QListWidgetItem *, QListWidgetItem *)), SLOT(slotShortcutItemChanged(QListWidgetItem *, QListWidgetItem *)));
    connect(ui.le_shortcutName, SIGNAL(textEdited(const QString &)), SLOT(slotShortcutNameChanged(const QString &)));
    connect(ui.le_shortcutPath, SIGNAL(textChanged(const QString &)), SLOT(slotShortcutPathChanged(const QString &)));
    connect(ui.pb_selectElement, SIGNAL(clicked()), SLOT(slotChooseElement()));
    connect(ui.buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(ui.buttonBox, SIGNAL(accepted()), this, SLOT(accept()));

    QSettings cfg("pwmdDialog");
    int size = cfg.beginReadArray("shortcuts");

    for (int i = 0; i < size; ++i) {
	cfg.setArrayIndex(i);
	QListWidgetItem *item = new QListWidgetItem(ui.lb_shortcuts);

	PwmdShortcut data = PwmdShortcut(cfg.value("name").toString(),
		cfg.value("path").toString());

	item->setData(Qt::UserRole, QVariant::fromValue(data));
	item->setText(data.name());
    }

    cfg.endArray();
    ui.sp_timeout->setValue(cfg.value("clipboardTimeout", 20).toInt());
}

void PwmdShortcutDialog::slotChooseElement()
{
    PwmdDialog *d = new PwmdDialog(socket, filename, "pwmdEdit");

    if (d->exec()) {
	QListWidgetItem *item = ui.lb_shortcuts->currentItem();
	PwmdShortcut data = item->data(Qt::UserRole).value<PwmdShortcut>();

	data.setPath(d->elementPath());
	item->setData(Qt::UserRole, QVariant::fromValue(data));
	ui.le_shortcutPath->setText(d->elementPath().replace("\t", "<TAB>", Qt::CaseInsensitive));
    }

    delete d;
}

PwmdShortcutDialog::~PwmdShortcutDialog()
{
    QSettings cfg("pwmdDialog");
    QList <QListWidgetItem *>items;

    items = ui.lb_shortcuts->findItems("*", Qt::MatchWildcard);
    cfg.beginWriteArray("shortcuts");

    for (int i = 0; i < items.size(); i++) {
	QListWidgetItem *item = items.at(i);
	PwmdShortcut data = item->data(Qt::UserRole).value<PwmdShortcut>();

	cfg.setArrayIndex(i);
	cfg.setValue("name", data.name());
	cfg.setValue("path", data.path().replace("<TAB>", "\t", Qt::CaseInsensitive));
    }

    cfg.endArray();
    cfg.setValue("clipboardTimeout", ui.sp_timeout->value());
}

void PwmdShortcutDialog::slotShortcutNameChanged(const QString &s)
{
    QListWidgetItem *item = ui.lb_shortcuts->currentItem();

    if (!item)
	return;

    PwmdShortcut data = item->data(Qt::UserRole).value<PwmdShortcut>();

    data.setName(s);
    item->setData(Qt::UserRole, QVariant::fromValue(data));
    item->setText(s);
}

void PwmdShortcutDialog::slotShortcutPathChanged(const QString &s)
{
    QListWidgetItem *item = ui.lb_shortcuts->currentItem();

    if (!item)
	return;

    PwmdShortcut data = item->data(Qt::UserRole).value<PwmdShortcut>();

    data.setPath(s);
    item->setData(Qt::UserRole, QVariant::fromValue(data));
}

void PwmdShortcutDialog::slotNewShortcut()
{
    ui.lb_shortcuts->addItem(new QListWidgetItem(tr("New shortcut")));
}

void PwmdShortcutDialog::slotRemoveShortcut()
{
    if (!ui.lb_shortcuts->currentItem())
	return;

    QListWidgetItem *item = ui.lb_shortcuts->takeItem(ui.lb_shortcuts->row(ui.lb_shortcuts->currentItem()));
    delete item;
}

void PwmdShortcutDialog::slotShortcutItemChanged(QListWidgetItem *item,
	QListWidgetItem *old)
{
    (void)old;

    ui.frame->setEnabled(item != NULL);

    if (!item) {
	ui.le_shortcutName->setText("");
	ui.le_shortcutPath->setText("");
	return;
    }

    PwmdShortcut data = item->data(Qt::UserRole).value<PwmdShortcut>();
    ui.le_shortcutName->setText(data.name());
    ui.le_shortcutPath->setText(data.path().replace("\t", "<TAB>", Qt::CaseInsensitive));
}
