/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef PWMDEDIT_H
#define PWMDEDIT_H

#include <QSystemTrayIcon>
#include <QMenu>
#include "pwmdDialog.h"
#include "pwmdShortcutDialog.h"

class PwmdEdit : public QApplication
{
    Q_OBJECT

    public:
	PwmdEdit(int argc, char **argv);
	~PwmdEdit() {};

    private slots:
	void slotSystemTrayActivated(QSystemTrayIcon::ActivationReason);
	void slotEditPwmd();
	void slotEditShortcuts();
	void slotShortCut(QAction *);
	void slotClearClipboard();

    private:
	void refreshShortcuts();

	QString filename;
	QString socket;
	QSystemTrayIcon *sti;
	int clipboardTimeout;
	QMenu *shortcutMenu;
	QTimer *clipboardTimer;
};

#endif
