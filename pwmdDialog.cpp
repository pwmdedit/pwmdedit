/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <QFileDialog>
#include <QColorDialog>
#include <QClipboard>
#include <QTimer>
#include <QCursor>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QMessageBox>
#include <QMenu>
#include <QContextMenuEvent>
#include <QSettings>
#include "pwmdDialog.h"
#include "pwmdTreeWidget.h"
#include "pwmdGenPassDialog.h"

PwmdDialog::PwmdDialog(const QString &s, const QString &f, const QString &name,
	QWidget *p) : QDialog(p)
{
    ui.setupUi(this);
    QSettings cfg("pwmdDialog");
    ipVersion = cfg.value("ipVersion", 0).toInt();

    switch (ipVersion) {
	case 0:
	    ui.rb_ipany->setChecked(true);
	    break;
	case 1:
	    ui.rb_ipv4->setChecked(true);
	    break;
	case 2:
	    ui.rb_ipv6->setChecked(true);
	    break;
    }

    connect(ui.rb_ipany, SIGNAL(toggled(bool)), SLOT(slotIpVersionToggled(bool)));
    connect(ui.rb_ipv4, SIGNAL(toggled(bool)), SLOT(slotIpVersionToggled(bool)));
    connect(ui.rb_ipv6, SIGNAL(toggled(bool)), SLOT(slotIpVersionToggled(bool)));
    ui.le_socket->setText(cfg.value("defaultSocket", "").toString());
    ui.le_hostname->setText(cfg.value("hostname", "").toString());
    ui.sp_port->setValue(cfg.value("port", 22).toInt());
    ui.le_username->setText(cfg.value("username", "").toString());
    ui.le_identity->setText(cfg.value("identity", "").toString());
    ui.le_knownhosts->setText(cfg.value("knownhosts", "").toString());
    ui.ck_remote->setChecked(cfg.value("remote", false).toBool());
    ui.sp_clipboardTimeout->setValue(cfg.value("clipboardTimeout", 20).toInt());
    clipboardTimer = new QTimer(this);
    connect(clipboardTimer, SIGNAL(timeout()), SLOT(slotClearClipboard()));

    if (ui.le_knownhosts->text().isEmpty())
	ui.le_knownhosts->setText(QDir::home().path() + "/.ssh/known_hosts");

    if (ui.le_identity->text().isEmpty())
	ui.le_identity->setText(QDir::home().path() + "/.ssh/pwmd");

    if (ui.le_username->text().isEmpty())
	ui.le_username->setText(getlogin());

    if (s.isEmpty())
	sock = updateSocket();
    else
	sock = s;

    connect(ui.ck_remote, SIGNAL(toggled(bool)), SLOT(slotRemoteToggled(bool)));

    ui.ck_reopenLastFile->setChecked(cfg.value("reopenLastFile", false).toBool());
    targetColor = cfg.value("targetColor", "DarkOrange").value<QColor>();
    invalidTargetColor = cfg.value("invalidTargetColor", "Red").value<QColor>();
    hilightColor = cfg.value("hilightColor", "Green").value<QColor>();

    if (f.isEmpty() && ui.ck_reopenLastFile->isChecked())
	_filename = cfg.value("lastOpenedFile", "").toString();
    else
	_filename = f;

    ui.ck_showContent->setChecked(cfg.value("showContent", true).toBool());
    ui.ck_geometry->setChecked(cfg.value("rememberGeometry", true).toBool());

    if (ui.ck_geometry->isChecked())
	setGeometry(cfg.value("geometry").toRect());

    pwm = new PwmdPrivate(sock, filename(), name, this);
    connect(pwm, SIGNAL(stateChanged(Pwmd::ConnectionState)), this, SLOT(slotConnectionStateChanged(Pwmd::ConnectionState)));
    connect(pwm, SIGNAL(connectionError(unsigned)), this, SLOT(slotConnectionError(unsigned)));
    pwm->setStatusCallback(statusCallback, this);
#if LIBPWMD_VERSION >= 0x603
    pwm->setKnownhostCallback(knownhostCallback, NULL);
#endif
    dndOperation = false;
    previousElement = NULL;
    previousRightPage = NULL;
    selectedElement = NULL;
    selectedAttribute = NULL;
    selectedFilename = QString::null;
    iterationsOrig = 0;
    ui.l_pwmdDialogVersion->setText(VERSION);
#if LIBPWMD_VERSION >= 0x603
    ui.l_libpwmdVersion->setText(pwmd_version());
#else
    ui.l_libpwmdVersion->setText(QString::number(LIBPWMD_VERSION, 16));
#endif
    ui.l_pwmdVersion->setText("-");
    ui.elementTree->setParent(this);
    ui.elementTree->header()->setResizeMode(0, QHeaderView::ResizeToContents);
    ui.elementTree->header()->setStretchLastSection(false);
    ui.elementTree->setItemDelegate(new PwmdTreeWidgetItemDelegate(ui.elementTree));
    QAction *act = new QAction(ui.elementTree);
    act->setShortcut(QKeySequence(tr("Ctrl+d")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotDeleteElement()));
    ui.elementTree->addAction(act);
    act = new QAction(ui.elementTree);
    act->setShortcut(QKeySequence(tr("Ctrl+r")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotNewRootElement()));
    ui.elementTree->addAction(act);
    act = new QAction(ui.elementTree);
    act->setShortcut(QKeySequence(tr("Ctrl+c")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotNewChildElement()));
    ui.elementTree->addAction(act);
    act = new QAction(ui.elementTree);
    act->setShortcut(QKeySequence(tr("Ctrl+m")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotRenameElement()));
    ui.elementTree->addAction(act);
    act = new QAction(ui.elementTree);
    act->setShortcut(QKeySequence(tr("Ctrl+e")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotEditAttributes()));
    ui.elementTree->addAction(act);
    act = new QAction(ui.elementTree);
    act->setShortcut(QKeySequence(tr("Ctrl+j")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotJumpToTarget()));
    ui.elementTree->addAction(act);
    act = new QAction(ui.elementTree);
    act->setShortcut(QKeySequence(tr("Ctrl+o")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotExecuteCommand()));
    ui.elementTree->addAction(act);
    act = new QAction(ui.elementTree);
    act->setShortcut(QKeySequence(tr("Ctrl+x")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotExpandElement()));
    ui.elementTree->addAction(act);
    act = new QAction(ui.elementTree);
    act->setShortcut(QKeySequence(tr("Ctrl+p")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotCollapseElement()));
    ui.elementTree->addAction(act);
    act = new QAction(ui.elementTree);
    act->setShortcut(QKeySequence(tr("Ctrl+y")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotClipboardElementPath()));
    ui.elementTree->addAction(act);

    act = new QAction(ui.attributeList);
    act->setShortcut(QKeySequence(tr("Ctrl+n")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotNewAttribute()));
    ui.attributeList->addAction(act);
    act = new QAction(ui.attributeList);
    act->setShortcut(QKeySequence(tr("Ctrl+d")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotDeleteAttribute()));
    ui.attributeList->addAction(act);
    act = new QAction(ui.attributeList);
    act->setShortcut(QKeySequence(tr("Ctrl+c")));
    connect(act, SIGNAL(triggered()), this, SLOT(slotClipboardAttribute()));
    ui.attributeList->addAction(act);

    connect(ui.elementTree, SIGNAL(itemEntered(QTreeWidgetItem *, int)), this, SLOT(slotElementEntered(QTreeWidgetItem *, int)));
    connect(ui.elementTree->itemDelegate(), SIGNAL(commitData(QWidget *)), this,
	    SLOT(slotRenameElementUpdate(QWidget *)));

    ui.openList->setParent(this);
    ui.attributeList->setParent(this);
    ui.stackedWidgetRight->setCurrentWidget(ui.contentPage);
    ui.progressBar->setEnabled(false);

    connect(ui.stackedWidgetRight, SIGNAL(currentChanged(int)), this, SLOT(slotStackedWidgetRightChanged(int)));
    connect(ui.cb_commandHistory, SIGNAL(currentIndexChanged(int)), this, SLOT(slotCommandHistoryChanged(int)));
    connect(ui.tb_search, SIGNAL(clicked()), this, SLOT(slotSearchCommandResults()));
    connect(ui.tb_aboutQt, SIGNAL(clicked()), this, SLOT(slotAboutQt()));
    ui.stackedWidgetRight->setEnabled(false);
    connect(ui.ck_inquire, SIGNAL(stateChanged(int)), this, SLOT(slotInquireStateChanged(int)));
    connect(ui.executeTextEdit, SIGNAL(textChanged()), SLOT(slotExecuteContentChanged()));
    connect(ui.tb_execute, SIGNAL(clicked()), this, SLOT(slotExecuteCommandOk()));
    connect(ui.tb_executeCancel, SIGNAL(clicked()), this, SLOT(slotExecuteCommandCancel()));
    connect(ui.tb_passwordClipboard, SIGNAL(clicked()), this, SLOT(slotClipboardPassword()));
    connect(ui.tb_targetColor, SIGNAL(clicked()), this, SLOT(slotChooseTargetColor()));
    connect(ui.tb_invalidTargetColor, SIGNAL(clicked()), this, SLOT(slotChooseInvalidTargetColor()));
    connect(ui.tb_hilightColor, SIGNAL(clicked()), this, SLOT(slotChooseHilightColor()));
    connect(ui.tb_clearCache, SIGNAL(clicked()), this, SLOT(slotClearCacheEntry()));
    connect(ui.le_password, SIGNAL(textEdited(const QString &)), this, SLOT(slotPasswordContentChanged(const QString &)));
    ui.onlyLineEdit->setValidator(new PwmdPasswordOnlyValidate());
    connect(ui.ck_showPassword, SIGNAL(clicked()), this, SLOT(slotShowPassword()));
    connect(ui.tb_generate, SIGNAL(clicked()), this, SLOT(slotGeneratePassword()));
    connect(ui.generateComboBox, SIGNAL(activated(int)), this, SLOT(slotGenerateTypeChanged(int)));
    connect(ui.ck_showAll, SIGNAL(clicked()), this, SLOT(slotShowAll()));
    connect(ui.ck_expandAll, SIGNAL(clicked()), this, SLOT(slotExpandAll()));
    connect(ui.ck_lock, SIGNAL(clicked()), this, SLOT(slotLock()));
    connect(ui.buttonBox, SIGNAL(accepted()), this, SLOT(slotFinished()));
    connect(ui.buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(ui.ck_showContent, SIGNAL(stateChanged(int)), SLOT(slotToggleShowContent(int)));
    connect(ui.pb_update, SIGNAL(clicked()), SLOT(slotSaveContent()));
    connect(ui.tb_passwordUpdate, SIGNAL(clicked()), SLOT(slotSaveContent()));
    connect(ui.textEdit, SIGNAL(textChanged()), SLOT(slotContentChanged()));
    connect(ui.elementTree, SIGNAL(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)),
	    this, SLOT(slotElementSelected(QTreeWidgetItem *, QTreeWidgetItem *)));
    connect(ui.attributeList, SIGNAL(currentItemChanged(QListWidgetItem *, QListWidgetItem *)),
	    this, SLOT(slotAttributeSelected(QListWidgetItem *, QListWidgetItem *)));
    connect(ui.openList, SIGNAL(currentItemChanged(QListWidgetItem *, QListWidgetItem *)),
	    this, SLOT(slotFilenameSelected(QListWidgetItem *, QListWidgetItem *)));
    connect(ui.openList, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this,
	    SLOT(slotFilenameDoubleClicked(QListWidgetItem *)));
    connect(ui.pb_connect, SIGNAL(clicked()), SLOT(slotConnectSocket()));

    connect(ui.tb_newElementOk, SIGNAL(clicked()), SLOT(slotNewChildElementOk()));
    connect(ui.tb_newElementCancel, SIGNAL(clicked()), SLOT(slotNewChildElementCancel()));

    connect(ui.pb_open, SIGNAL(clicked()), SLOT(slotOpenFile()));
    connect(ui.tb_openOk, SIGNAL(clicked()), SLOT(slotOpenOk()));
    connect(ui.tb_openCancel, SIGNAL(clicked()), SLOT(slotOpenCancel()));
    connect(ui.tb_openNew, SIGNAL(clicked()), SLOT(slotOpenNew()));
    connect(ui.tb_newFilenameOk, SIGNAL(clicked()), SLOT(slotOpenNewOk()));
    connect(ui.tb_newFilenameCancel, SIGNAL(clicked()), SLOT(slotOpenNewCancel()));

    connect(ui.tb_attrOk, SIGNAL(clicked()), SLOT(slotAttributesOk()));
    connect(ui.tb_newAttributeOk, SIGNAL(clicked()), SLOT(slotNewAttributeOk()));
    connect(ui.tb_newAttributeCancel, SIGNAL(clicked()), SLOT(slotNewAttributeCancel()));
    connect(ui.newAttributeValueTextEdit, SIGNAL(textChanged()), SLOT(slotNewAttributeValueChanged()));

    connect(ui.pb_options, SIGNAL(clicked()), SLOT(slotEditOptions()));
    connect(ui.tb_optionsOk, SIGNAL(clicked()), SLOT(slotOptionsOk()));
    connect(ui.tb_optionsCancel, SIGNAL(clicked()), SLOT(slotOptionsCancel()));
    connect(ui.pb_identity, SIGNAL(clicked()), SLOT(slotChooseIdentityFile()));
    connect(ui.pb_knownhosts, SIGNAL(clicked()), SLOT(slotChooseKnownhostsFile()));

    QPushButton *pb = ui.buttonBox->button(QDialogButtonBox::Apply);
    connect(pb, SIGNAL(clicked()), SLOT(slotSaveDocument()));
    pb->setEnabled(false);
    pb = ui.buttonBox->button(QDialogButtonBox::Reset);
    connect(pb, SIGNAL(clicked()), SLOT(slotReset()));
    pb->setEnabled(false);
    init = true;
}

PwmdDialog::~PwmdDialog()
{
    delete pwm;
    QSettings cfg("pwmdDialog");

    cfg.setValue("defaultSocket", ui.le_socket->text());
    cfg.setValue("reopenLastFile", ui.ck_reopenLastFile->isChecked());
    cfg.setValue("lastOpenedFile", filename());
    cfg.setValue("invalidTargetColor", invalidTargetColor);
    cfg.setValue("targetColor", targetColor);
    cfg.setValue("hilightColor", hilightColor);
    cfg.setValue("showContent", ui.ck_showContent->isChecked());
    cfg.setValue("rememberGeometry", ui.ck_geometry->isChecked());
    cfg.setValue("geometry", geometry());
    cfg.setValue("remote", ui.ck_remote->isChecked());
    cfg.setValue("hostname", ui.le_hostname->text());
    cfg.setValue("port", ui.sp_port->value());
    cfg.setValue("username", ui.le_username->text());
    cfg.setValue("identity", ui.le_identity->text());
    cfg.setValue("knownhosts", ui.le_knownhosts->text());
    cfg.setValue("ipVersion", ipVersion);
    cfg.setValue("clipboardTimeout", ui.sp_clipboardTimeout->value());
}

void PwmdDialog::slotClearClipboard()
{
    clearClipboard();
}

void PwmdDialog::clearClipboard()
{
    QClipboard *c = QApplication::clipboard();

    c->setText("", QClipboard::Selection);
    c->setText("");
}

void PwmdDialog::closeEvent(QCloseEvent *ev)
{
    (void)ev;
    doFinished();
}

void PwmdDialog::slotIpVersionToggled(bool b)
{
    (void)b;

    if (ui.rb_ipany->isChecked())
	ipVersion = 0;
    else if (ui.rb_ipv4->isChecked())
	ipVersion = 1;
    else if (ui.rb_ipv6->isChecked())
	ipVersion = 2;
}

void PwmdDialog::slotChooseIdentityFile()
{
    QString f = QFileDialog::getOpenFileName(this,
	    tr("SSH Identity file"), QDir::home().path());

    if (f.isEmpty())
	return;

    ui.le_identity->setText(f);
}

void PwmdDialog::slotChooseKnownhostsFile()
{
    QString f = QFileDialog::getOpenFileName(this,
	    tr("SSH knownhosts file"), QDir::home().path());

    if (f.isEmpty())
	return;

    ui.le_knownhosts->setText(f);
}

void PwmdDialog::slotRemoteToggled(bool b)
{
    ui.le_socket->setEnabled(!b);
}

QString PwmdDialog::updateSocket()
{
    if (ui.ck_remote->isChecked()) {
	QString s = "ssh";
	
	switch (ipVersion) {
	    case 0:
		break;
	    case 1:
		s.append("4");
		break;
	    case 2:
		s.append("6");
		break;
	}
	
	s.append("://");

	if (!ui.le_username->text().isEmpty()) {
	    s.append(ui.le_username->text());
	    s.append('@');
	}

	s.append(ui.le_hostname->text());
	s.append(':');
	s.append(QString("%1").arg(ui.sp_port->value()));
	s.append(',');
	s.append(ui.le_identity->text());
	s.append(',');
	s.append(ui.le_knownhosts->text());
	return s;
    }

    return ui.le_socket->text();
}

void PwmdDialog::slotConnectionError(unsigned rc)
{
    showError(rc);
}

void PwmdDialog::slotConnectionStateChanged(Pwmd::ConnectionState s)
{
    state = s;

    switch (s) {
	case Pwmd::Init:
	    setConnected(false);
	    break;
	case Pwmd::Connected:
	    setConnected();
	    break;
	case Pwmd::Opened:
	    setOpen(true);
	    break;
    }
}

void PwmdDialog::slotStackedWidgetRightChanged(int n)
{
    if (n == -1)
	return;

    if (ui.stackedWidgetRight->widget(n) != ui.contentPage)
	return;

    if (!ui.elementTree->topLevelItemCount()) {
	setContent("");
	ui.stackedWidgetRight->setEnabled(false);
    }
}

void PwmdDialog::slotElementEntered(QTreeWidgetItem *item, int col)
{
    (void)col;
    QBrush br;

    if (item && item == previousElement)
	return;

    if (previousElement) {
	br = previousElement->foreground(0);
	br.setColor(previousElementColor);
	previousElement->setForeground(0, br);
    }

    if (!item || !dndOperation) {
	previousElement = NULL;
	return;
    }

    br = item->foreground(0);
    previousElementColor = br.color();
    br.setColor(hilightColor);
    item->setForeground(0, br);
    previousElement = item;
}

void PwmdDialog::slotCommandHistoryChanged(int idx)
{
    if (idx == -1)
	return;

    ui.executeTextEdit->setPlainText(ui.cb_commandHistory->itemText(idx));
}

void PwmdDialog::slotSearchCommandResults()
{
    if (ui.le_search->text().isEmpty())
	return;

    ui.resultTextEdit->find(ui.le_search->text());
}

void PwmdDialog::slotAboutQt()
{
    QMessageBox::aboutQt(this, tr("About Qt"));
}

void PwmdDialog::slotRenameElementUpdate(QWidget *w)
{
    QLineEdit *le = qobject_cast<QLineEdit *>(w);
    int n = selectedElementPath.lastIndexOf("\t")+1;
    QString origName = selectedElementPath.mid(n);

    if (origName.at(0) == '!')
	origName.remove(0, 1);

    if (le->text().isEmpty()) {
	selectedElement->setText(0, origName);
	return;
    }

    if (origName == le->text())
	return;

    gpg_error_t rc;
    pwm->command(QString("RENAME %1 %2").arg(selectedElementPath).arg(le->text()), rc);

    if (rc) {
	selectedElement->setText(0, origName);
	return;
    }

    setModified();
    refreshElementTree();
}

void PwmdDialog::slotInquireStateChanged(int state)
{
    bool b = state == Qt::Unchecked ? false : true;

    ui.le_inquire->setEnabled(b);
}

void PwmdDialog::slotExecuteContentChanged()
{
    tabify(ui.executeTextEdit);
}

void PwmdDialog::slotExecuteCommand()
{
    if (!verifyNewContent())
	return;

    ui.stackedWidgetRight->setEnabled(true);
    previousRightPage = ui.stackedWidgetRight->currentWidget();
    ui.stackedWidgetRight->setCurrentWidget(ui.executePage);
}

void PwmdDialog::slotExecuteCommandOk()
{
    QString data = ui.executeTextEdit->toPlainText().replace("<TAB>", "\t", Qt::CaseInsensitive);
    QString result = QString::null;

    ui.resultTextEdit->setPlainText("");

    if (data.isEmpty())
	return;
    
    if (ui.cb_commandHistory->findText(ui.executeTextEdit->toPlainText()) == -1)
	ui.cb_commandHistory->addItem(ui.executeTextEdit->toPlainText());

    ui.cb_commandHistory->setEnabled(true);
    gpg_error_t rc;

    if (ui.ck_inquire->isChecked())
	rc = pwm->inquire(ui.le_inquire->text().toUtf8(), data);
    else
	result = pwm->command(data, rc);

    if (rc)
	return;

    ui.resultTextEdit->setPlainText(result);
    setModified();
    refreshElementTree();
}

void PwmdDialog::slotExecuteCommandCancel()
{
    ui.stackedWidgetRight->setCurrentWidget(previousRightPage);
    elementSelected(selectedElement);
}

void PwmdDialog::slotFilenameDoubleClicked(QListWidgetItem *item)
{
    slotFilenameSelected(item, NULL);
    ui.tb_openOk->animateClick();
}

void PwmdDialog::paintEvent(QPaintEvent *ev)
{
    (void)ev;

    if (init) {
	init = false;
	QTimer::singleShot(1, this, SLOT(slotEditElements()));
    }
}

void PwmdDialog::slotChooseHilightColor()
{
    QColor c = QColorDialog::getColor(hilightColor);

    if (!c.isValid())
	return;

    hilightColor.setRgba(c.rgba());
    ui.tb_hilightColor->setPalette(QPalette(hilightColor));
}

void PwmdDialog::slotChooseTargetColor()
{
    QColor c = QColorDialog::getColor(targetColor);

    if (!c.isValid())
	return;

    targetColor.setRgba(c.rgba());
    ui.tb_targetColor->setPalette(QPalette(targetColor));

    foreach (QTreeWidgetItem *item, ui.elementTree->findItems("*",
		Qt::MatchRecursive|Qt::MatchWildcard)) {
	if (hasTarget(item) && !badTarget(item)) {
	    QBrush br = item->foreground(0);

	    br.setColor(targetColor);
	    item->setForeground(0, br);
	}
    }
}

void PwmdDialog::slotChooseInvalidTargetColor()
{
    QColor c = QColorDialog::getColor(invalidTargetColor);

    if (!c.isValid())
	return;

    invalidTargetColor.setRgba(c.rgba());
    ui.tb_invalidTargetColor->setPalette(QPalette(invalidTargetColor));

    foreach (QTreeWidgetItem *item, ui.elementTree->findItems("*",
		Qt::MatchRecursive|Qt::MatchWildcard)) {
	if (badTarget(item)) {
	    QBrush br = item->foreground(0);

	    br.setColor(invalidTargetColor);
	    item->setForeground(0, br);
	}
    }
}

void PwmdDialog::slotClearCacheEntry()
{
    if (filename().isEmpty())
	return;

    gpg_error_t rc;
    pwm->command(QString("CLEARCACHE %1").arg(filename()), rc);
}

void PwmdDialog::slotShowPassword()
{
    bool b = ui.ck_showPassword->isChecked();

    ui.le_password->setEchoMode(!b ? QLineEdit::Password : QLineEdit::Normal);
}

void PwmdDialog::setModified(bool b)
{
    ui.buttonBox->button(QDialogButtonBox::Apply)->setEnabled(b);
    setWindowTitle(QString("PwmdEdit: %1%2").arg(filename()).arg(b ? "*" : ""));
}

void PwmdDialog::slotClipboardPassword()
{
    QClipboard *c = QApplication::clipboard();

    c->setText(ui.le_password->text(), QClipboard::Selection);
    c->setText(ui.le_password->text());

    if (ui.sp_clipboardTimeout->value())
	clipboardTimer->start(ui.sp_clipboardTimeout->value()*1000);
}

void PwmdDialog::slotAttributesOk()
{
    if (!verifyNewContent())
	return;

    if (!modifiedTarget) {
	ui.stackedWidgetLeft->setCurrentWidget(ui.elementPage);
	slotElementSelected(selectedElement, 0);
	return;
    }

    slotEditElements();
}

void PwmdDialog::slotLock()
{
    bool b = ui.ck_lock->isChecked();
    gpg_error_t rc;

    pwm->setLockOnOpen(b);
    pwm->command(b ? "LOCK" : "UNLOCK", rc);
}

void PwmdDialog::slotShowAll()
{
    unsigned total = 0, n = 0;

    setBusy();

    foreach (QTreeWidgetItem *items, ui.elementTree->findItems("*",
		Qt::MatchRecursive|Qt::MatchWildcard)) {
	(void)items;
	total++;
    }

    if (total > 100) {
	ui.progressBar->setMaximum(total);
	ui.progressBar->setFormat(tr("Building view %p%"));
    }

    foreach (QTreeWidgetItem *items, ui.elementTree->findItems("*",
		Qt::MatchRecursive|Qt::MatchWildcard)) {
	QTreeWidgetItem *parent = items->parent();

	if (total > 100 && !(++n % 50))
	    ui.progressBar->setValue(n);

	foreach (QTreeWidgetItem *item, ui.elementTree->findItems("*",
		    Qt::MatchRecursive|Qt::MatchWildcard)) {
	    if (item == items)
		continue;

	    if (badTarget(item) || (!hasTarget(item) &&
			item->text(0) == items->text(0) &&
			parent == item->parent()))
		item->setHidden(!ui.ck_showAll->isChecked());
	}
    }

    setBusy(false);
    ui.elementTree->scrollToItem(selectedElement, QAbstractItemView::PositionAtCenter);
}

void PwmdDialog::slotExpandAll()
{
    QTreeWidgetItem *item;
    unsigned total = 0, n = 0;

    setBusy();

    foreach (item, ui.elementTree->findItems("*",
		Qt::MatchRecursive|Qt::MatchWildcard))
	total++;

    if (total > 100) {
	ui.progressBar->setMaximum(total);
	ui.progressBar->setFormat(tr("Building view %p%"));
    }

    foreach (item, ui.elementTree->findItems("*",
		Qt::MatchRecursive|Qt::MatchWildcard)) {
	item->setExpanded(ui.ck_expandAll->isChecked());

	if (total > 100 && !(++n % 50))
	    ui.progressBar->setValue(n);
    }

    item = selectedElement;

    while (item) {
	item->setExpanded(true);
	item = item->parent();
    }

    setBusy(false);
    ui.elementTree->scrollToItem(selectedElement, QAbstractItemView::PositionAtCenter);
}

void PwmdDialog::slotEditOptions()
{
    remote = ui.ck_remote->isChecked();

    if (state != Pwmd::Init) {
	gpg_error_t rc;
	QString data = pwm->command("GETCONFIG iterations", rc, true, false);

	if (!rc) {
	    iterationsOrig = data.toInt(NULL);
	    ui.sp_iterations->setValue(iterationsOrig);
	}

	data = pwm->command("VERSION", rc, true, false);

	if (!rc)
	    ui.l_pwmdVersion->setText(data);
    }

    slotRemoteToggled(remote);
    ui.tb_targetColor->setPalette(QPalette(targetColor));
    ui.tb_invalidTargetColor->setPalette(QPalette(invalidTargetColor));
    ui.tb_hilightColor->setPalette(QPalette(hilightColor));
    ui.stackedWidgetRight->setHidden(true);
    ui.bottomFrame->setHidden(true);
    ui.stackedWidgetLeft->setCurrentWidget(ui.optionsPage);
}

void PwmdDialog::slotOptionsOk()
{
    bool mod = false;

    if (state != Pwmd::Init) {
	if (iterationsOrig != (unsigned)ui.sp_iterations->value()) {
#if LIBPWMD_VERSION >= 0x603
	    pwmd_setopt(pwm->handle(), PWMD_OPTION_ITERATIONS,
		    ui.sp_iterations->value());
#else
	    gpg_error_t rc;

	    pwm->command(QString("SET iterations=%1").arg(ui.sp_iterations->value()), rc);

	    if (rc) {
		showError(rc);
		return;
	    }
#endif
	    mod = true;
	}
    }

    if (mod)
	setModified();

    ui.stackedWidgetRight->setEnabled(selectedElement != NULL);
    ui.stackedWidgetLeft->setCurrentWidget(ui.elementPage);

    if (sock != updateSocket()) {
	if (doFinished(false)) {
	    sock = updateSocket();
	    pwm->reset();
	    pwm->setSocket(sock);
	    slotConnectSocket();
	}
    }

    ui.bottomFrame->setHidden(false);
    ui.stackedWidgetRight->setHidden(false);
}

void PwmdDialog::slotOptionsCancel()
{
    slotRemoteToggled(remote);
    ui.ck_remote->setChecked(remote);
    ui.bottomFrame->setHidden(false);
    ui.stackedWidgetRight->setHidden(false);
    ui.stackedWidgetLeft->setCurrentWidget(ui.elementPage);
}

QString PwmdDialog::filename()
{
    return _filename;
}

QString PwmdDialog::socket()
{
    return sock;
}

void PwmdDialog::slotFilenameSelected(QListWidgetItem *item, QListWidgetItem *old)
{
    (void)old;

    if (!item)
	return;

    ui.tb_openOk->setEnabled(true);
    selectedFilename = item->text();
}

void PwmdDialog::slotOpenOk()
{
    if (selectedFilename.isEmpty())
	return;

    _filename = selectedFilename;
    state = state != Pwmd::Init ? Pwmd::Connected : state;
    gpg_error_t rc = pwm->open();

    if (rc) {
	setOpen(false);
	return;
    }

    editElements();
    ui.stackedWidgetRight->setEnabled(selectedElement != NULL);
}

void PwmdDialog::slotOpenNewCancel()
{
    ui.stackedWidgetRight->setEnabled(false);
    ui.stackedWidgetLeft->setEnabled(true);
    ui.stackedWidgetRight->setCurrentWidget(previousRightPage);
}

void PwmdDialog::slotOpenNewOk()
{
    if (!ui.le_newFilename->text().isEmpty()) {
	ui.stackedWidgetLeft->setEnabled(true);
	selectedFilename = ui.le_newFilename->text();
	slotOpenOk();
    }
    else {
	ui.stackedWidgetRight->setEnabled(false);
	ui.stackedWidgetRight->setCurrentWidget(previousRightPage);
    }

    ui.stackedWidgetLeft->setEnabled(true);
}

void PwmdDialog::slotOpenNew()
{
    ui.le_newFilename->setText("");
    ui.stackedWidgetLeft->setEnabled(false);
    ui.stackedWidgetRight->setEnabled(true);
    ui.le_newFilename->setFocus();
    previousRightPage = ui.stackedWidgetRight->currentWidget();
    ui.stackedWidgetRight->setCurrentWidget(ui.newFilenamePage);
}

void PwmdDialog::slotOpenCancel()
{
    ui.pb_open->setEnabled(true);
    ui.stackedWidgetRight->setEnabled(selectedElement != NULL);
    ui.stackedWidgetLeft->setCurrentWidget(ui.elementPage);
}

void PwmdDialog::slotOpenFile()
{
    if (!doFinished(false))
	return;

    ui.stackedWidgetRight->setEnabled(false);
    ui.stackedWidgetLeft->setCurrentWidget(ui.openPage);
    ui.openList->setFocus();
    ui.tb_openOk->setEnabled(false);
    QString f = filename();
    _filename = QString::null;
    slotRefreshFilenameTree();
    _filename = f;
}

QList<QListWidgetItem *> PwmdDialog::buildFilenameTree(gpg_error_t &rc)
{
    QList<QListWidgetItem *> items;
    QString tmp = pwm->command("LS", rc, true, false);

    if (rc)
	return items;

    QStringList files = tmp.split("\n");

    for (int file = 0; file < files.count(); file++) {
	QListWidgetItem *item;

	item = new QListWidgetItem(files.at(file));
	items.append(item);
    }

    return items;
}

void PwmdDialog::slotRefreshFilenameTree()
{
    gpg_error_t rc;
    QList<QListWidgetItem *> items = buildFilenameTree(rc);

    if (rc) {
	showError(rc);
	return;
    }

    ui.openList->clear();

    foreach (QListWidgetItem *item, items)
	ui.openList->addItem(item);
}

bool PwmdDialog::doFinished(bool done)
{
    if (!ui.pb_update->isEnabled() &&
	!ui.buttonBox->button(QDialogButtonBox::Apply)->isEnabled() &&
	!ui.tb_passwordUpdate->isEnabled()) {

	if (done)
	    accept();

	return true;
    }
    else if (ui.pb_update->isEnabled() || ui.tb_passwordUpdate->isEnabled()) {
	gpg_error_t rc = doSaveContent();

	if (rc) {
	    showError(rc);
	    return false;
	}
    }

    QMessageBox msgBox;
    msgBox.setText(tr("Some elements have been modified but have yet to be stored in the data file."));
    msgBox.setInformativeText(tr("Do you want to permanately save the changes?"));
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();

    if (ret == QMessageBox::Cancel)
	return false;

    if (ret == QMessageBox::Save) {
	if (saveDocument())
	    return false;
    }

    if (done)
	accept();

    setModified(false);
    return true;
}

void PwmdDialog::slotFinished()
{
    doFinished();
}

void PwmdDialog::slotPasswordContentChanged(const QString &str)
{
    (void)str;
    bool b = ui.le_password->isModified();

    ui.tb_passwordUpdate->setEnabled(b);
    setModified();
}

void PwmdDialog::tabify(QPlainTextEdit *w)
{
    QString s = w->toPlainText();
    int pos = w->textCursor().position();
    int n = s.count('\t');

    if (!n)
	return;

    s.replace("\t", "<TAB>", Qt::CaseInsensitive);
    w->document()->setPlainText(s);
    QTextCursor t = w->textCursor();
    pos += n*4;
    t.setPosition(pos, QTextCursor::MoveAnchor);
    w->setTextCursor(t);
}

void PwmdDialog::slotContentChanged()
{
    // Undo
    if (!ui.textEdit->document()->isModified()) {
	ui.pb_update->setEnabled(false);
	return;
    }

    if (ui.ck_showContent->isChecked())
	ui.pb_update->setEnabled(true);

    if (ui.stackedWidgetLeft->currentWidget() == ui.attributePage) {
	if (selectedAttribute->text() == "target")
	    tabify(ui.textEdit);
    }

    setModified();
}

void PwmdDialog::setBusy(bool b)
{
    if (b)
	setCursor(Qt::WaitCursor);
    else {
	ui.progressBar->setValue(0);
	ui.progressBar->setFormat("Ready");
	unsetCursor();
    }
}

gpg_error_t PwmdDialog::saveDocument()
{
    gpg_error_t rc = pwm->save();

    if (rc && rc != GPG_ERR_CANCELED) {
	showError(rc);
	return rc;
    }
    else if (!rc)
	setModified(false);

    return rc;
}

void PwmdDialog::slotSaveDocument()
{
    if (ui.tb_passwordUpdate->isEnabled() || ui.pb_update->isEnabled()) {
	gpg_error_t rc = doSaveContent();

	if (rc) {
	    showError(rc);
	    return;
	}
    }

    saveDocument();
}

void PwmdDialog::showError(gpg_error_t rc)
{
    qWarning("PWMD ERROR %u: %s", rc, pwmd_strerror(rc));
    QMessageBox m;

    m.setText(tr("There was an error while communicating with pwmd."));
    m.setInformativeText(QString("ERR %1: %2").arg(rc).arg(pwmd_strerror(rc)));

    switch (rc) {
	case EPWMD_FILE_MODIFIED:
	    m.setDetailedText(tr("The currently open data file has been externally modified; either by another client or by some other means. Reopen the file to fix this error."));
	    break;
	case GPG_ERR_LOCKED:
	    m.setDetailedText(tr("Another client has the same data file opened and has requested that other clients must wait until it has finished."));
	    break;
	case GPG_ERR_ELOOP:
	    m.setDetailedText(tr("A recursion loop was detected while processing a command. This happens when an element with a \"target\" attribute references itself. If the error was caused by a DnD operation then the last operation will be undone and the problem fixed. Otherwise you can either Reset your changes and lose them, or remove the offending \"target\" attribute manually by sending the DUMP command and locating the element that contains the self-referencing \"target\" attribute."));
	    break;
	case GPG_ERR_ASS_CONNECT_FAILED:
	    if (state == Pwmd::Init)
		m.setDetailedText(tr("There was a problem connecting to the pwmd server. Be sure that it is running and that the correct socket location was specified in the Options."));
	    else
		m.setDetailedText(tr("There was a problem connecting to the pinentry program."));

	    break;
	default:
	    break;
    }

    m.setIcon(QMessageBox::Critical);
    m.exec();
}

gpg_error_t PwmdDialog::doSaveContent()
{
    QString data = QString("%1").arg(elementPath(selectedElement));
    gpg_error_t rc;
    
    if (ui.stackedWidgetLeft->currentWidget() == ui.elementPage) {
	data.append("\t");

	if (ui.stackedWidgetRight->currentWidget() == ui.passwordContentPage) {
	    if (!ui.le_password->text().isEmpty())
		data.append(ui.le_password->text());
	}
	else {
	    if (!ui.textEdit->toPlainText().isEmpty())
		data.append(ui.textEdit->toPlainText());
	}

	rc = pwm->inquire("STORE", data);
    }
    else {
	if (!selectedAttribute || !(selectedAttribute->flags() & Qt::ItemIsEnabled)) {
	    ui.pb_update->setEnabled(false);
	    ui.textEdit->document()->setModified(false);
	    return 0;
	}

	QString cmd = QString("ATTR SET %1 %2 %3").arg(selectedAttribute->text()).arg(data).arg(ui.textEdit->toPlainText().replace("<TAB>", "\t", Qt::CaseInsensitive));

	pwm->command(cmd, rc);
    }

    if (!rc) {
	ui.pb_update->setEnabled(false);
	ui.tb_passwordUpdate->setEnabled(false);
	ui.textEdit->document()->setModified(false);
	ui.le_password->setModified(false);

	if (ui.stackedWidgetRight->currentWidget() == ui.passwordContentPage)
	    ui.lengthSpinBox->setValue(ui.le_password->text().length());

	setModified();
    }

    return rc;
}

void PwmdDialog::slotSaveContent()
{
    if (!selectedElement)
	return;

    doSaveContent();
}

void PwmdDialog::slotReset()
{
    gpg_error_t rc;
    pwm->command("RESET", rc);

    if (!rc) {
	state = Pwmd::Connected;
	setConnected();
	rc = pwm->open();
	
	if (!rc) {
	    ui.stackedWidgetRight->setCurrentWidget(ui.contentPage);
	    editElements();
	}
    }
}

void PwmdDialog::setOpen(bool b)
{
    selectedElement = NULL;
    selectedAttribute = NULL;
    setWindowTitle(QString("PwmdEdit: %1").arg(!b ? "-" : filename()));
    ui.elementGroupBox->setEnabled(b);
    ui.updateFrame->setEnabled(b);
    ui.tb_clearCache->setEnabled(b);
    ui.sp_iterations->setEnabled(b);
    setContent("");
    ui.stackedWidgetRight->setCurrentWidget(ui.contentPage);
    ui.elementTree->clear();
    ui.elementTree->setFocus();
    ui.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(b);
    ui.buttonBox->button(QDialogButtonBox::Reset)->setEnabled(b);
}

void PwmdDialog::setConnected(bool b)
{
    setModified(false);

    if (!b) {
	setOpen(false);
	ui.l_cached->setText("-");
	ui.l_clients->setText("-");
	ui.l_pwmdVersion->setText("-");
    }

    ui.serverStatusGroupBox->setEnabled(b);
    ui.updateFrame->setEnabled(false);
    ui.progressBar->setEnabled(b);
    ui.stackedWidgetLeft->setEnabled(true);
    ui.elementGroupBox->setEnabled(false);
    ui.stackedWidgetRight->setEnabled(false);
    ui.pb_connect->setEnabled(!b);
    ui.pb_open->setEnabled(b);
    ui.pb_open->setFocus();
    ui.progressBar->setValue(0);
    ui.progressBar->setFormat("");
    ui.tb_passwordUpdate->setEnabled(false);

    QPushButton *pb = ui.buttonBox->button(QDialogButtonBox::Ok);
    pb->setEnabled(b);
}

void PwmdDialog::slotRefreshElementTree()
{
    refreshElementTree();
}

gpg_error_t PwmdDialog::refreshElementTree(bool target)
{
    gpg_error_t rc;
    QList<QTreeWidgetItem *> items = buildElementTree(NULL, rc);

    if (rc) {
	if (rc == GPG_ERR_ELOOP && target) {
	    QString cmd = QString("ATTR DELETE target %1").arg(elementPath(selectedElement));
	    pwm->command(cmd, rc);
	}

	return rc;
    }

    QString path = elementPath(selectedElement);
    ui.elementTree->clear();
    ui.elementTree->insertTopLevelItems(0, items);
    ui.elementTree->sortByColumn(0, Qt::AscendingOrder);
    selectedElement = findElement(path);

    if (!selectedElement)
	setContent("");

    if (ui.stackedWidgetRight->currentWidget() == ui.contentPage)
	slotStackedWidgetRightChanged(ui.stackedWidgetRight->indexOf(ui.contentPage));

    ui.elementTree->setCurrentItem(selectedElement);
    slotShowAll();

    if (ui.ck_expandAll->isChecked())
	slotExpandAll();

    ui.elementTree->scrollToItem(selectedElement, QAbstractItemView::PositionAtCenter);
    return 0;
}

void PwmdDialog::slotConnectSocket()
{
    setBusy();
    gpg_error_t rc = pwm->connect();

    if (rc) {
	setBusy(false);
	showError(rc);
	return;
    }

    if (filename().isEmpty()) {
	slotOpenFile();
	setBusy(false);
	return;
    }

    setBusy(false);
    rc = refreshElementTree();
}

#if LIBPWMD_VERSION >= 0x603
gpg_error_t PwmdDialog::knownhostCallback(void *data, const char *host,
	const char *key, size_t len)
{
    (void)key;
    (void)len;
    (void)data;
    QMessageBox msgBox;
    msgBox.setText(QString(tr("While attempting an SSH connection to %1, there was a problem verifying it's host key against the known and trusted hosts file because it's host key was not found.\n\nWould you like to treat this connection as trusted for this and future connections by adding %2's host key to the known hosts file?")).arg(host).arg(host));

    msgBox.setIcon(QMessageBox::Question);
    msgBox.setInformativeText(tr("Accept this host key as trusted?"));
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int n = msgBox.exec();

    if (n == QMessageBox::Cancel)
	return GPG_ERR_CANCELED;

    return 0;
}
#endif

int PwmdDialog::statusCallback(void *data, const char *line)
{
    PwmdDialog *w = static_cast<PwmdDialog *>(data);
    QStringList l = QString(line).split(" ");
    pwm_t *h = w->pwm->handle();
    gpg_error_t rc;
    pwmd_socket_t type;
    
    rc = pwmd_socket_type(h, &type);

    if (!rc && type == PWMD_SOCKET_LOCAL &&
	    l.at(0) != "ENCRYPT" && l.at(0) != "DECRYPT" && l.at(0) != "CACHE"
	    && l.at(0) != "CLIENTS")
	return 0;

    if (l.at(0) == "DECOMPRESS") {
	w->ui.progressBar->setMaximum(l.at(2).toULong(NULL));
	w->ui.progressBar->setValue(l.at(1).toULong(NULL));
	w->ui.progressBar->setFormat(tr("Decompressing %p%"));
    }
    else if (l.at(0) == "COMPRESS") {
	w->ui.progressBar->setMaximum(l.at(2).toULong(NULL));
	w->ui.progressBar->setValue(l.at(1).toULong(NULL));
	w->ui.progressBar->setFormat(tr("Compressing %p%"));
    }
    else if (l.at(0) == "ENCRYPT") {
	w->ui.progressBar->setMaximum(l.at(2).toULong(NULL));
	w->ui.progressBar->setValue(l.at(1).toULong(NULL));
	w->ui.progressBar->setFormat(tr("Encrypting %p%"));
    }
    else if (l.at(0) == "DECRYPT") {
	w->ui.progressBar->setMaximum(l.at(2).toULong(NULL));
	w->ui.progressBar->setValue(l.at(1).toULong(NULL));
	w->ui.progressBar->setFormat(tr("Decrypting %p%"));
    }
    else if (l.at(0) == "XFER") {
	w->ui.progressBar->setMaximum(l.at(2).toULong(NULL));
	w->ui.progressBar->setValue(l.at(1).toULong(NULL));
	w->ui.progressBar->setFormat(tr("Transferring - %v of %m %p%"));
    }
    else if (l.at(0) == "LOCKED") {
	w->ui.progressBar->setMaximum(0);
	w->ui.progressBar->setValue(0);
	w->ui.progressBar->setFormat(tr("Waiting for lock"));
    }
    else if (l.at(0) == "CACHE")
	w->ui.l_cached->setText(l.at(1));
    else if (l.at(0) == "CLIENTS")
	w->ui.l_clients->setText(l.at(1));

    return 0;
}

void PwmdDialog::slotToggleShowContent(int state)
{
    int s = state == Qt::Checked ? true : false;

    if (s && ui.pb_update->isEnabled())
	return;

    if (!s && !verifyNewContent()) {
	ui.ck_showContent->setChecked(true);
	return;
    }

    ui.textEdit->setEnabled(ui.stackedWidgetRight->currentWidget() != ui.openPage && s);

    if (!s) {
	ui.textEdit->document()->setPlainText("");
	ui.textEdit->document()->setModified(false);
    }
    else if (ui.stackedWidgetLeft->currentWidget() == ui.elementPage)
	slotElementSelected(selectedElement, 0);
    else
	slotAttributeSelected(selectedAttribute, 0);
}

bool PwmdDialog::verifyNewContent()
{
    if (!ui.tb_passwordUpdate->isEnabled() && !ui.pb_update->isEnabled())
	return true;

    if (ui.ck_autoUpdate->isChecked()) {
	if (doSaveContent()) {
	    ui.elementTree->setCurrentItem(selectedElement);
	    ui.attributeList->setCurrentItem(selectedAttribute);
	    return false;
	}

	ui.pb_update->setEnabled(false);
	ui.tb_passwordUpdate->setEnabled(false);
	return true;
    }

    QMessageBox msgBox;
    msgBox.setText(tr("The content of the current element has changed."));
    msgBox.setInformativeText(tr("Do you want to keep the changes?"));
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int n = msgBox.exec();

    if (n == QMessageBox::Cancel) {
	ui.elementTree->setCurrentItem(selectedElement);
	ui.attributeList->setCurrentItem(selectedAttribute);
	return false;
    }
    else if (n == QMessageBox::Save) {
	if (doSaveContent()) {
	    ui.elementTree->setCurrentItem(selectedElement);
	    ui.attributeList->setCurrentItem(selectedAttribute);
	    return false;
	}
    }
    else {
	setPasswordContent("");
	ui.tb_passwordUpdate->setEnabled(false);
	ui.pb_update->setEnabled(false);
	ui.textEdit->document()->setModified(false);
    }

    return true;
}

bool PwmdDialog::elementSelected(QTreeWidgetItem *item)
{
    if (!item || item->isDisabled())
	return false;

    if (!verifyNewContent())
	return false;

    selectedElement = item;
    ui.elementTree->scrollToItem(selectedElement);

    if (!ui.ck_showContent->isChecked()) {
	setContent("");
	return true;
    }

    ui.stackedWidgetRight->setEnabled(true);
    QString path = elementPath(item);
    gpg_error_t rc;

    if (path.isEmpty())
	return false;

    QString s = pwm->command(QString("GET %1").arg(path), rc);

    if (rc) {
	setContent("");
	return false;
    }

    PwmdTreeWidgetItemData data = item->data(0, Qt::UserRole).value<PwmdTreeWidgetItemData>();

    if (data.hasPassword()) {
	pwm->command(QString("ATTR GET password %1").arg(path), rc, false);

	if (rc && rc != GPG_ERR_NOT_FOUND)
	    showError(rc);
	else if (!rc) {
	    if (ui.stackedWidgetRight->currentWidget() != ui.executePage)
		ui.stackedWidgetRight->setCurrentWidget(ui.passwordContentPage);

	    setPasswordContent(s);
	    ui.lengthSpinBox->setValue(s.isEmpty() ? 16 : s.length());
	    return true;
	}
	else if (rc) {
	    data.setHasPassword(false);
	    item->setData(0, Qt::UserRole, QVariant::fromValue(data));
	}
    }

    if (ui.stackedWidgetRight->currentWidget() != ui.executePage)
	ui.stackedWidgetRight->setCurrentWidget(ui.contentPage);

    setContent(s);
    return true;
}

void PwmdDialog::slotElementSelected(QTreeWidgetItem *item, QTreeWidgetItem *old)
{
    (void)old;

    if (!item || item == old)
	return;

    item->sortChildren(0, Qt::AscendingOrder);
    elementSelected(item);
}

void PwmdDialog::slotAttributeSelected(QListWidgetItem *item, QListWidgetItem *old)
{
    (void)old;

    if (!item || !verifyNewContent())
	return;

    if (!(item->flags() & Qt::ItemIsEnabled)) {
	ui.attributeList->setCurrentItem(selectedAttribute);
	return;
    }

    selectedAttribute = item;
    ui.stackedWidgetRight->setEnabled(true);

    if (!ui.ck_showContent->isChecked()) {
	setContent("");
	return;
    }

    QString s = elementPath(selectedElement);

    if (s.isEmpty())
	return;

    gpg_error_t rc;
    s = pwm->command(QString("ATTR GET %1 %2").arg(item->text()).arg(s), rc);

    if (rc) {
	setContent("");
	return;
    }

    setContent(s.replace("\t", "<TAB>", Qt::CaseInsensitive));
}

void PwmdDialog::setPasswordContent(const QString &str)
{
    ui.le_password->setText(str);
    ui.le_password->setModified(false);
}

void PwmdDialog::setContent(const QString &str)
{
    disconnect(ui.textEdit, SIGNAL(textChanged()), this, SLOT(slotContentChanged()));
    ui.textEdit->document()->setPlainText(str);
    ui.textEdit->document()->setModified(false);
    connect(ui.textEdit, SIGNAL(textChanged()), SLOT(slotContentChanged()));
}

bool PwmdDialog::hasBadTarget(QTreeWidgetItem *item, const QString &name,
	gpg_error_t &rc)
{
    rc = 0;

    if (name.at(0) == '!')
	return false;

    QString path = pwm->command(QString("REALPATH %1").arg(elementPath(item)), rc, false);
    bool b = false;

    if (rc == GPG_ERR_ELEMENT_NOT_FOUND) {
	rc = 0;
	b = true;
    }
    else if (rc) {
	showError(rc);
	b = false;
    }
    else {
	PwmdTreeWidgetItemData data = item->data(0, Qt::UserRole).value<PwmdTreeWidgetItemData>();

	data.setTarget(path);
	item->setData(0, Qt::UserRole, QVariant::fromValue(data));
    }

    if (b) {
	QBrush br = item->foreground(0);

	br.setColor(invalidTargetColor);
	item->setForeground(0, br);
	item->setDisabled(true);
    }

    return b;
}

bool PwmdDialog::hasTarget(const QTreeWidgetItem *item)
{
    PwmdTreeWidgetItemData data = item->data(0, Qt::UserRole).value<PwmdTreeWidgetItemData>();

    return data.hasTarget();
}

bool PwmdDialog::badTarget(const QTreeWidgetItem *item)
{
    PwmdTreeWidgetItemData data = item->data(0, Qt::UserRole).value<PwmdTreeWidgetItemData>();

    return data.badTarget();
}

void PwmdDialog::setBadTarget(QTreeWidgetItem *item, bool b)
{
    PwmdTreeWidgetItemData data = item->data(0, Qt::UserRole).value<PwmdTreeWidgetItemData>();

    data.setBadTarget(b);
    item->setData(0, Qt::UserRole, QVariant::fromValue(data));
    return;
}

QString PwmdDialog::elementName(const QTreeWidgetItem *item, bool real)
{
    return QString("%1%2").arg(hasTarget(item) && !real ? "" : "!").arg(item->text(0));
}

void PwmdDialog::setItemText(QTreeWidgetItem *item, const QString &str)
{
    bool hasTarget = false;

    if (str.at(0) != '!')
	hasTarget = true;

    item->setText(0, !hasTarget ? str.right(str.length()-1) : str);
    PwmdTreeWidgetItemData data = PwmdTreeWidgetItemData(hasTarget, str);
    item->setData(0, Qt::UserRole, QVariant::fromValue(data));

    if (hasTarget) {
	QBrush br = item->foreground(0);

	br.setColor(targetColor);
	item->setForeground(0, br);
    }

    item->setFlags(item->flags() | Qt::ItemIsEditable);
}

QList<QTreeWidgetItem *>PwmdDialog::buildElementTree(const QString &path,
	gpg_error_t &rc)
{
    QList<QTreeWidgetItem *> elementTree;
    QString tmp = pwm->command(QString("LIST %1").arg(path.isEmpty() ? "" : path), rc);

    if (rc || tmp.isEmpty())
	return elementTree;

    QStringList roots;

    if (path.isEmpty())
	roots = QString(tmp).split("\n");
    else {
	QStringList t = QString(tmp).split("\n");
	roots.append(t.at(0));
    }

    for (int root = 0; root < roots.count(); root++) {
	QTreeWidgetItem *item;
	bool badTarget = false;

	item = new QTreeWidgetItem((QTreeWidget *)0);
	setItemText(item, roots.at(root));
	elementTree.append(item);
	badTarget = hasBadTarget(item, roots.at(root), rc);

	if (rc)
	    goto fail;

	if (badTarget) {
	    ui.ck_showAll->setChecked(true);
	    setBadTarget(item);
	    continue;
	}

	tmp = pwm->command(QString("LIST %1").arg(roots.at(root)), rc);

	if (rc)
	    goto fail;

	QStringList lines = QString(tmp).split("\n");

	for (int line = 0; line < lines.count(); line++) {
	    QStringList elements = QString(lines.at(line)).split("\t");
	    QTreeWidgetItem *parent = elementTree.at(root);
	    bool nomatch = true;
	    QTreeWidgetItem *item;
	    int element = 1;

	    while ((item = parent->child(0))) {
		if (elements.at(element) == elementName(item)) {
		    element++;
		    parent = item;
		    continue;
		}
		else if (element+1 == elements.count())
		    break;

		for (int i = 0; i < parent->childCount(); i++) {
		    QTreeWidgetItem *tmp = parent->child(i);

		    if (elementName(parent->child(i)) == elements.at(element)) {
			parent = tmp;
			element++;
			nomatch = false;
			break;
		    }
		}

		if (nomatch)
		    break;

		nomatch = true;
	    }

	    if (nomatch) {
		for (; element < elements.count(); element++) {
		    parent = new QTreeWidgetItem(parent);
		    setItemText(parent, elements.at(element));
		    badTarget = hasBadTarget(parent, elements.at(element), rc);

		    if (rc)
			goto fail;

		    if (badTarget) {
			ui.ck_showAll->setChecked(true);
			setBadTarget(parent);
		    }
		}
	    }
	}
    }

fail:
    return elementTree;
}

QString PwmdDialog::elementPath(const QTreeWidgetItem *item, const QChar &sep)
{
    QString path = QString::null;

    if (!item)
	return path;

    for (;;) {
	PwmdTreeWidgetItemData data = item->data(0, Qt::UserRole).value<PwmdTreeWidgetItemData>();

	if (!item->parent()) {
	    path.prepend(data.name());
	    break;
	}

	path.prepend(QString("%1%2").arg(sep).arg(data.name()));
	item = item->parent();
    }

    return path;
}

QString PwmdDialog::elementPath(const QChar &sep)
{
    return elementPath(selectedElement, sep);
}

void PwmdDialog::slotNewChildElement()
{
    ui.le_newElement->setText("");

    if (!selectedElement || !verifyNewContent())
	return;

    ui.le_newElement->setFocus();
    previousRightPage = ui.stackedWidgetRight->currentWidget();
    ui.stackedWidgetRight->setCurrentWidget(ui.newElementPage);
}

void PwmdDialog::slotNewChildElementOk()
{
    if (!selectedElement)
	return;

    if (ui.le_newElement->text().isEmpty()) {
	ui.stackedWidgetRight->setCurrentWidget(previousRightPage);
	return;
    }

    QString root = elementPath(selectedElement);
    QString data;

    if (!root.isEmpty())
	data.append(QString("%1\t").arg(root));

    data.append(QString("%1").arg(ui.le_newElement->text()));
    ui.stackedWidgetRight->setCurrentWidget(previousRightPage);
    data.append("\t");
    gpg_error_t rc = pwm->inquire("STORE", data);

    if (!rc) {
	setModified();
	refreshElementTree();
    }
}

void PwmdDialog::slotNewChildElementCancel()
{
    ui.stackedWidgetRight->setCurrentWidget(previousRightPage);
    ui.stackedWidgetRight->setEnabled(selectedElement != NULL);
}

void PwmdDialog::slotNewRootElement()
{
    ui.le_newElement->setText("");

    if (!verifyNewContent())
	return;

    ui.le_newElement->setFocus();
    previousRightPage = ui.stackedWidgetRight->currentWidget();
    disconnect(ui.tb_newElementOk, SIGNAL(clicked()), this, SLOT(slotNewChildElementOk()));
    connect(ui.tb_newElementOk, SIGNAL(clicked()), this, SLOT(slotNewRootElementOk()));
    ui.stackedWidgetRight->setCurrentWidget(ui.newElementPage);
    ui.stackedWidgetRight->setEnabled(true);
}

void PwmdDialog::slotNewRootElementOk()
{
    if (!ui.le_newElement->text().isEmpty()) {
	gpg_error_t rc = pwm->inquire("STORE", ui.le_newElement->text());

	if (!rc) {
	    ui.stackedWidgetRight->setCurrentWidget(ui.contentPage);
	    setModified();
	    refreshElementTree();
	}
    }

    disconnect(ui.tb_newElementOk, SIGNAL(clicked()), this, SLOT(slotNewRootElementOk()));
    connect(ui.tb_newElementOk, SIGNAL(clicked()), this, SLOT(slotNewChildElementOk()));
}

void PwmdDialog::slotDeleteElement()
{
    if (!selectedElement || !verifyNewContent())
	return;

    gpg_error_t rc;
    pwm->command(QString("DELETE %1").arg(elementPath(selectedElement)), rc);

    if (!rc) {
	setModified();
	QTreeWidgetItem *orig = selectedElement;
	selectedElement = ui.elementTree->itemBelow(orig);

	if (!selectedElement)
	    selectedElement = ui.elementTree->itemAbove(orig);

	refreshElementTree();
    }
}

void PwmdDialog::slotRenameElement()
{
    if (!selectedElement || !verifyNewContent())
	return;

    selectedElementPath = elementPath(selectedElement);
    ui.elementTree->editItem(selectedElement);
}

gpg_error_t PwmdDialog::editElements()
{
    if (!verifyNewContent())
	return 0;

    ui.stackedWidgetLeft->setCurrentWidget(ui.elementPage);
    return refreshElementTree();
}

void PwmdDialog::slotEditElements()
{
    if (filename().isEmpty()) {
	gpg_error_t rc = pwm->connect(false);

	if (rc)
	    showError(rc);

	return;
    }

    editElements();
}

void PwmdDialog::slotEditLiteralAttributes()
{
    QString path = elementPath(selectedElement);
    int n = path.lastIndexOf('\t')+1;

    path.insert(n, '!');
    selectedElement = findElement(path, false);
    ui.tb_editLiteralAttributes->setText(tr("Edit tar&get"));
    disconnect(ui.tb_editLiteralAttributes, SIGNAL(clicked()), this, SLOT(slotEditLiteralAttributes()));
    connect(ui.tb_editLiteralAttributes, SIGNAL(clicked()), this, SLOT(slotEditTargetAttributes()));
    refreshAttributeList(selectedElement);
}

void PwmdDialog::slotEditTargetAttributes()
{
    QString path = elementPath(selectedElement);
    int n = path.lastIndexOf('\t')+1;

    path.remove(n, 1);
    selectedElement = findElement(path, false);
    ui.tb_editLiteralAttributes->setText(tr("Edit &literal"));
    disconnect(ui.tb_editLiteralAttributes, SIGNAL(clicked()), this, SLOT(slotEditTargetAttributes()));
    connect(ui.tb_editLiteralAttributes, SIGNAL(clicked()), this, SLOT(slotEditLiteralAttributes()));
    refreshAttributeList(selectedElement);
}

void PwmdDialog::slotEditAttributes()
{
    modifiedTarget = false;

    if (!verifyNewContent())
	return;

    refreshAttributeList(selectedElement);
    ui.stackedWidgetRight->setEnabled(false);
    ui.stackedWidgetLeft->setCurrentWidget(ui.attributePage);
    ui.stackedWidgetRight->setCurrentWidget(ui.contentPage);
    ui.tb_editLiteralAttributes->setText(tr("Edit &literal"));
    ui.tb_editLiteralAttributes->setEnabled(hasTarget(selectedElement));
    disconnect(ui.tb_editLiteralAttributes, SIGNAL(clicked()), this, SLOT(slotEditTargetAttributes()));
    connect(ui.tb_editLiteralAttributes, SIGNAL(clicked()), this, SLOT(slotEditLiteralAttributes()));
    ui.attributeList->setFocus();
}

QList<QListWidgetItem *> PwmdDialog::buildAttributeList(QTreeWidgetItem *element, gpg_error_t &rc)
{
    QList<QListWidgetItem *> items;
    QString tmp = pwm->command(QString("ATTR LIST %1").arg(elementPath(element)), rc);

    if (rc || tmp.isEmpty())
	return items;

    QStringList attrs = tmp.split("\n");

    for (int attr = 0; attr < attrs.count(); attr++) {
	QListWidgetItem *item;
	int n = attrs.at(attr).indexOf(QChar(' '));
	QString s;

	if (n == -1)
	    s.append(attrs.at(attr));
	else
	    s.append(attrs.at(attr).left(n));

	item = new QListWidgetItem(s);
	items.append(item);

	if (s == "_name")
	    item->setFlags(~(item->flags() & Qt::ItemIsEnabled));
    }

    return items;
}

void PwmdDialog::refreshAttributeList(QTreeWidgetItem *element)
{
    if (!element)
	return;

    gpg_error_t rc;
    QList<QListWidgetItem *> items = buildAttributeList(element, rc);

    if (rc) {
	showError(rc);
	return;
    }

    setContent("");
    selectedAttribute = NULL;
    ui.attributeList->clear();

    foreach (QListWidgetItem *item, items)
	ui.attributeList->addItem(item);

    ui.stackedWidgetRight->setEnabled(false);
}

void PwmdDialog::slotNewAttributeValueChanged()
{
    if (ui.le_newAttributeName->text() == "target")
	tabify(ui.newAttributeValueTextEdit);
}

void PwmdDialog::slotNewAttributeCancel()
{
    ui.stackedWidgetRight->setCurrentWidget(ui.contentPage);
    ui.stackedWidgetRight->setEnabled(selectedAttribute != NULL);
    ui.stackedWidgetLeft->setEnabled(true);
}

void PwmdDialog::slotNewAttributeOk()
{
    QString name = ui.le_newAttributeName->text();
    QString value = ui.newAttributeValueTextEdit->toPlainText();
    QString cmd;
    gpg_error_t rc;

    ui.stackedWidgetLeft->setEnabled(true);

    if (name.isEmpty())
	goto done;
    else if (name == "_name")
	goto done;

    cmd = QString("ATTR SET %1 %2 %3").arg(name).arg(elementPath(selectedElement)).arg(value).replace("<TAB>", "\t", Qt::CaseInsensitive);
    pwm->command(cmd, rc);

    if (!rc) {
	if (name == "target") {
	    modifiedTarget = true;
	    ui.tb_editLiteralAttributes->setEnabled(true);
	}
	else if (name == "password") {
	    PwmdTreeWidgetItemData data = selectedElement->data(0, Qt::UserRole).value<PwmdTreeWidgetItemData>();
	    data.setHasPassword(true);
	    selectedElement->setData(0, Qt::UserRole, QVariant::fromValue(data));
	}

	setModified();
	ui.attributeList->addItem(new QListWidgetItem(name));
    }

done:
    ui.stackedWidgetRight->setCurrentWidget(ui.contentPage);
    ui.stackedWidgetRight->setEnabled(selectedAttribute != NULL);
}

void PwmdDialog::slotNewAttribute()
{
    ui.le_newAttributeName->setText("");
    ui.newAttributeValueTextEdit->document()->setPlainText("");

    if (!verifyNewContent())
	return;

    ui.le_newAttributeName->setFocus();
    ui.stackedWidgetLeft->setEnabled(false);
    ui.stackedWidgetRight->setEnabled(true);
    ui.stackedWidgetRight->setCurrentWidget(ui.newAttributePage);
}

void PwmdDialog::slotDeleteAttribute()
{
    if (!selectedAttribute || !(selectedAttribute->flags() & Qt::ItemIsEnabled))
	return;

    if (!verifyNewContent())
	return;

    gpg_error_t rc;
    pwm->command(QString("ATTR DELETE %1 %2").arg(selectedAttribute->text()).arg(elementPath(selectedElement)), rc);

    if (!rc) {
	if (selectedAttribute->text() == "target") {
	    ui.tb_editLiteralAttributes->setEnabled(false);
	    modifiedTarget = true;
	}
	else if (selectedAttribute->text() == "password") {
	    PwmdTreeWidgetItemData data = selectedElement->data(0, Qt::UserRole).value<PwmdTreeWidgetItemData>();
	    data.setHasPassword(false);
	    selectedElement->setData(0, Qt::UserRole, QVariant::fromValue(data));
	}

	setModified();
	setContent("");
	ui.attributeList->takeItem(ui.attributeList->row(selectedAttribute));

	if (ui.attributeList->count() == 1)
	    ui.stackedWidgetRight->setEnabled(false);
    }
}

void PwmdDialog::slotJumpToTarget()
{
    PwmdTreeWidgetItemData data = selectedElement->data(0, Qt::UserRole).value<PwmdTreeWidgetItemData>();

    if (!data.hasTarget())
	return;

    QTreeWidgetItem *item = findElement(data.target());

    if (item) {
	ui.elementTree->setCurrentItem(item);
	selectedElement = item;

	if (selectedElement->isHidden()) {
	    ui.ck_showAll->setChecked(true);
	    slotShowAll();
	}

	ui.elementTree->scrollToItem(selectedElement, QAbstractItemView::PositionAtCenter);
    }
}

QTreeWidgetItem *PwmdDialog::findElement(const QString &path, bool real)
{
    QStringList elements = path.split("\t");
    int element = 0;

    for (int root = 0; root < ui.elementTree->topLevelItemCount(); root++) {
	QTreeWidgetItem *item = ui.elementTree->topLevelItem(root);

	if (elementName(item, real) == elements.at(element)) {
	    element++;

	    while (element < elements.count()) {
		for (int n = 0; n < item->childCount(); n++) {
		    if (elementName(item->child(n), real) == elements.at(element)) {
			item = item->child(n);
			break;
		    }
		}

		element++;
	    }

	    return item;
	}
    }

    return NULL;
}

void PwmdDialog::slotClipboardAttribute()
{
    QClipboard *c = QApplication::clipboard();

    c->setText(selectedAttribute->text(), QClipboard::Selection);
    c->setText(selectedAttribute->text());
}

void PwmdDialog::slotClipboardElementPath()
{
    QClipboard *c = QApplication::clipboard();

    c->setText(elementPath(selectedElement), QClipboard::Selection);
    c->setText(elementPath(selectedElement));
}

void PwmdDialog::slotDndCopyElement()
{
    gpg_error_t rc;
    QString srcPath = elementPath(selectedElement);
    QString dst = elementPath(dropElement);
    QString cmd;
    int n = srcPath.lastIndexOf('\t')+1;

    if (dst.isEmpty())
	cmd = QString("COPY %1 %2").arg(srcPath).arg(srcPath.mid(n));
    else {
	cmd = QString("COPY %1 %2").arg(srcPath).arg(dst);
	cmd.append(QString("\t%1").arg(srcPath.mid(n)));
    }

    pwm->command(cmd, rc);

    if (!rc) {
	setModified();
	refreshElementTree();
    }
}

void PwmdDialog::slotDndMoveElement()
{
    if (dropElement == selectedElement ||
	    isParentOf(selectedElement, dropElement))
	return;

    QString cmd = QString("MOVE %1 ").arg(elementPath(selectedElement));

    if (dropElement)
	cmd.append(QString("%1").arg(elementPath(dropElement)));

    gpg_error_t rc;
    pwm->command(cmd, rc);

    if (!rc) {
	setModified();
	refreshElementTree();
    }
}

void PwmdDialog::slotDndCreateTarget()
{
    if (!dropElement || (dropElement == selectedElement))
	return;

    gpg_error_t rc;
    QString cmd = QString("ATTR SET target %1 %2").arg(elementPath(selectedElement)).arg(elementPath(dropElement));
    pwm->command(cmd, rc);

    if (!rc) {
	setModified();
	refreshElementTree(true);
    }
}

bool PwmdDialog::isParentOf(QTreeWidgetItem *a, QTreeWidgetItem *b)
{
    if (!a || !b)
	return false;

    while (b) {
	b = b->parent();

	if (b == a)
	    return true;
    }

    return false;
}

static void recurseExpand(QTreeWidgetItem *item, bool b)
{
    item->setExpanded(b);

    for (int i = 0; i < item->childCount(); i++) {
	QTreeWidgetItem *tmp = item->child(i);

	recurseExpand(tmp, b);
    }
}

void PwmdDialog::slotExpandElement()
{
    setBusy();
    recurseExpand(selectedElement, true);
    setBusy(false);
}

void PwmdDialog::slotCollapseElement()
{
    recurseExpand(selectedElement, false);
}

void PwmdDialog::popupContextMenu()
{
    selectedElement = ui.elementTree->currentItem();
    selectedAttribute = ui.attributeList->currentItem();

    QMenu *m = new QMenu(this);
    QAction *a = m->addAction(tr("New &root"), this, SLOT(slotNewRootElement()));

    a = m->addAction(tr("New &child"), this, SLOT(slotNewChildElement()));

    if (!selectedElement)
	a->setEnabled(false);

    a = m->addAction(tr("Rena&me"), this, SLOT(slotRenameElement()));

    if (!selectedElement)
	a->setEnabled(false);

    a = m->addAction(tr("&Delete"), this, SLOT(slotDeleteElement()));

    if (!selectedElement)
	a->setEnabled(false);

    m->addSeparator();
    a = m->addAction(tr("&Edit attributes"), this, SLOT(slotEditAttributes()));

    if (!selectedElement)
	a->setEnabled(false);

    a = m->addAction(tr("&Jump to target"), this, SLOT(slotJumpToTarget()));

    if (!selectedElement)
	a->setEnabled(false);

    if (selectedElement && !hasTarget(selectedElement))
	a->setEnabled(false);

    m->addSeparator();
    a = m->addAction(tr("C&ommand"), this, SLOT(slotExecuteCommand()));
    m->addSeparator();
    a = m->addAction(tr("E&xpand"), this, SLOT(slotExpandElement()));

    if (!selectedElement)
	a->setEnabled(false);

    a = m->addAction(tr("Colla&pse"), this, SLOT(slotCollapseElement()));

    if (!selectedElement)
	a->setEnabled(false);

    a = m->addAction(tr("Cop&y element path"), this, SLOT(slotClipboardElementPath()));

    if (!selectedElement)
	a->setEnabled(false);

    m->exec(QCursor::pos());
    delete m;
}

void PwmdDialog::slotGeneratePassword()
{
    QString result = QString::null;

    do {
	switch (ui.generateComboBox->currentIndex()) {
	    case 0:
		result = PwmdGenPassDialog::generate(PwmdGenPassDialog::Any,
			ui.lengthSpinBox->value());
		break;
	    case 1:
		result = PwmdGenPassDialog::generate(PwmdGenPassDialog::AlphaNumeric,
			ui.lengthSpinBox->value());
		break;
	    case 2:
		result = PwmdGenPassDialog::generate(PwmdGenPassDialog::Alpha,
			ui.lengthSpinBox->value());
		break;
	    case 3:
		result = PwmdGenPassDialog::generate(PwmdGenPassDialog::Numeric,
		       	ui.lengthSpinBox->value());
		break;
	    case 4:
		if (ui.onlyLineEdit->text().isEmpty())
		    break;

		result = PwmdGenPassDialog::generate(ui.onlyLineEdit->text(),
			ui.lengthSpinBox->value());
		break;
	    default:
		break;
	}
    } while (!QString::compare(result, ui.le_password->text()) &&
	    ui.onlyLineEdit->text().length() > 1);

    if (result.isEmpty())
	return;

    ui.le_password->selectAll();
    ui.le_password->insert(result);
    ui.tb_passwordUpdate->setEnabled(true);
    slotClipboardPassword();
}

void PwmdDialog::slotGenerateTypeChanged(int idx)
{
    bool b = false;

    if (idx == ui.generateComboBox->count()-1)
	b = true;

    ui.onlyLineEdit->setEnabled(b);
}
