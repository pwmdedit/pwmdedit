/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <QMenu>
#include "pwmdDialog.h"
#include "pwmdTreeWidget.h"

PwmdTreeWidget::PwmdTreeWidget(QWidget *p) : QTreeWidget(p)
{
    lastItem = NULL;
    expandTimer = new QTimer(this);
    expandTimer->setSingleShot(true);
    connect(expandTimer, SIGNAL(timeout()), this, SLOT(slotExpandTimeout()));
}

void PwmdTreeWidget::setParent(PwmdDialog *p)
{
    parent = p;
}

void PwmdTreeWidget::keyPressEvent(QKeyEvent *ev)
{
    // Alt-Enter
    if (ev->key() == Qt::Key_Return && ev->nativeModifiers() == 8) {
	QCursor::setPos(mapToGlobal(QPoint(visualItemRect(currentItem()).topRight())));
	parent->popupContextMenu();
	return;
    }

    QTreeWidget::keyPressEvent(ev);
}

void PwmdTreeWidget::mouseReleaseEvent(QMouseEvent *ev)
{
    QTreeWidget::mouseReleaseEvent(ev);

    if (ev->button() != Qt::RightButton)
	return;

    if (parent->selectedElement != parent->ui.elementTree->currentItem()) {
	if (!parent->elementSelected(parent->ui.elementTree->currentItem()))
	    return;
    }

    parent->popupContextMenu();
}

void PwmdTreeWidget::mousePressEvent(QMouseEvent *ev)
{
    dragStartPosition = ev->pos();
    QTreeWidget::mousePressEvent(ev);
}

void PwmdTreeWidget::mouseDoubleClickEvent(QMouseEvent *ev)
{
    parent->selectedElementPath = parent->elementPath(parent->selectedElement);
    QTreeWidget::mouseDoubleClickEvent(ev);
}

bool PwmdTreeWidget::isValidDndMove(QTreeWidgetItem *drop)
{
    if (parent->isParentOf(parent->selectedElement, drop) ||
	    parent->selectedElement == drop || (!parent->selectedElement->parent() && !drop))
	return false;

    return true;
}

void PwmdTreeWidget::mouseMoveEvent(QMouseEvent *ev)
{
    dndMove = false;

    if ((ev->pos()-dragStartPosition).manhattanLength()
	    < QApplication::startDragDistance())
	return;

    QDrag *drag = new QDrag(this);
    QMimeData *m = new QMimeData;
    m->setText(parent->elementPath(parent->selectedElement));
    drag->setMimeData(m);
    Qt::DropAction action;

    if ((ev->buttons() & Qt::LeftButton) && ev->modifiers()) {
	parent->dndOperation = true;

	switch (ev->modifiers()) {
	    case Qt::ControlModifier:
		action = drag->exec(Qt::CopyAction);
		parent->dndOperation = false;
		parent->slotElementEntered(NULL, 0);

		if (drag->target() && drag->target()->parentWidget() == parent->ui.elementTree && action == Qt::CopyAction)
		    parent->slotDndCopyElement();

		break;
	    case Qt::AltModifier:
		action = drag->exec(Qt::LinkAction);
		parent->dndOperation = false;
		parent->slotElementEntered(NULL, 0);

		if (drag->target() && drag->target()->parentWidget() == parent->ui.elementTree && action == Qt::LinkAction && parent->dropElement)
		    parent->slotDndCreateTarget();

		break;
	    default:
		parent->dndOperation = false;
		break;
	}

	return;
    }
    else if ((ev->buttons() & Qt::RightButton)) {
	parent->dndOperation = true;
	drag->exec(Qt::CopyAction);
	parent->dndOperation = false;
	parent->slotElementEntered(NULL, 0);

	if (drag->target() && drag->target()->parentWidget() != parent->ui.elementTree)
	    return;

	QMenu *p = new QMenu(this);
	QAction *a;

	a = p->addAction(tr("&Copy here"), parent, SLOT(slotDndCopyElement()));
	a = p->addAction(tr("&Move here"), parent, SLOT(slotDndMoveElement()));

	if (!isValidDndMove(parent->dropElement))
	    a->setEnabled(false);

	a = p->addAction(tr("Set as &target"), parent, SLOT(slotDndCreateTarget()));

	if (!parent->dropElement)
	    a->setEnabled(false);

	p->exec(QCursor::pos());
	delete p;
    }
    else if ((ev->buttons() & Qt::LeftButton)) {
	// Fake a move action. Would be good if Qt would let us get the cursor
	// displayed while doing a Qt::MoveAction since we're doing a
	// Qt::CopyAction. Oh well.
	//drag->setDragCursor(moveactionpixmap, Qt::CopyAction);
	dndMove = true;
	parent->dndOperation = true;
	action = drag->exec(Qt::CopyAction);
	parent->dndOperation = false;
	parent->slotElementEntered(NULL, 0);

	if (drag->target() && drag->target()->parentWidget() == parent->ui.elementTree && action == Qt::CopyAction)
	    parent->slotDndMoveElement();
    }
}

void PwmdTreeWidget::dragMoveEvent(QDragMoveEvent *ev)
{
    QTreeWidget::dragMoveEvent(ev);

    if (!parent->selectedElement) {
	ev->ignore();
	return;
    }

    QTreeWidgetItem *item = parent->ui.elementTree->itemAt(ev->pos());

    if (ev->dropAction() == Qt::LinkAction) {
	if (!item || item == parent->selectedElement) {
	    expandTimer->stop();
	    ev->ignore();
	    return;
	}

	if (lastItem == item) {
	    if (!expandTimer->isActive())
		expandTimer->start(500);

	    ev->accept();
	    return;
	}
    }

    parent->slotElementEntered(item, 0);
    expandTimer->stop();

    if (dndMove) {
	if (!isValidDndMove(item)) {
	    if (item) {
		lastItem = item;
		expandTimer->start(500);
	    }

	    ev->ignore();
	    return;
	}
    }

    if (item)
	expandTimer->start(500);

    lastItem = item;
    ev->accept();
}

void PwmdTreeWidget::slotExpandTimeout()
{
    lastItem->setExpanded(true);
}

void PwmdTreeWidget::dragEnterEvent(QDragEnterEvent *ev)
{
    if (ev->source() != parent->ui.elementTree)
	return;

    if (ev->mimeData()->hasFormat("text/plain"))
	ev->acceptProposedAction();
}

// Can't do the actual modifiying of elements here in case of pwmd error the
// elementTree would already be modified. This only happens with
// Qt::MoveAction for some reason.
bool PwmdTreeWidget::dropMimeData(QTreeWidgetItem *dst, int index,
	const QMimeData *data, Qt::DropAction action)
{
    (void)index;
    (void)data;
    (void)action;

    expandTimer->stop();
    parent->dropElement = dst;
    return true;
}
 
QStringList PwmdTreeWidget::mimeTypes() const
{
    QStringList l;

    l.append("text/plain");
    return l;
}
 
Qt::DropActions PwmdTreeWidget::supportedDropActions() const
{
    return Qt::MoveAction|Qt::CopyAction|Qt::LinkAction;
}
