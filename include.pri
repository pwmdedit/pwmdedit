# Include this in your main project.pro which uses the PwmdDialog class.

CONFIG += link_pkgconfig
PKGCONFIG += libpwmd
DEFINES += HAVE_LIBPWMD

*-g++*:QMAKE_CFLAGS += -g -O2 -I/usr/local/include
*-g++*:QMAKE_LDFLAGS += -g -O2 -L/usr/local/lib

HEADERS += $$PWD/pwmdDialog.h \
	   $$PWD/pwmdTreeWidget.h \
	   $$PWD/pwmdListWidget.h \
	   $$PWD/pwmdGenPassDialog.h \
	   $$PWD/pwmd.h

SOURCES += $$PWD/pwmdDialog.cpp \
	   $$PWD/pwmdGenPassDialog.cpp \
	   $$PWD/pwmdTreeWidget.cpp \
	   $$PWD/pwmdListWidget.cpp \
	   $$PWD/pwmd.cpp

INTERFACES += $$PWD/pwmdDialog.ui \
	      $$PWD/pwmdGenPassDialog.ui
