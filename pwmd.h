/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
/*
 * This is a generic class that wraps around the most used libpwmd functions.
 * It should serve its purpose for most applications and when it doesn't you
 * can reimplement it or use the handle() to do what's needed.
 */
#ifndef PWMD_H
#define PWMD_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <libpwmd.h>

class Pwmd;
class PwmdStatusMessageThread : public QThread
{
    public:
	PwmdStatusMessageThread(Pwmd *p)
	{
	    parent = p;
	};

	void run();

    private:
	Pwmd *parent;
};

class Pwmd : public QObject
{
    Q_OBJECT

    public:
	// 'sock' is the Url to connect to. 'filename' is the default filename
	// to open which can be reset with setFilename(). 'clientName' is used
	// with the pwmd log file.
	Pwmd(const QString &sock = 0, const QString &filename = 0,
		const QString &clientName = 0);
	~Pwmd();

	// Sent as an argument to the stateChanged() signal.
	typedef enum {
	    Init, Connected, Opened
	} ConnectionState;

	// The current pwmd connection state.
	ConnectionState state();

	// For status messages. When no command is active then the
	// PwmdStatusMessageThread takes care of idle status messages.
	typedef int (*StatusCallback)(void *data, const char *line);

	// The data parameter is user data passed to the callback.
	gpg_error_t setStatusCallback(StatusCallback cb, void *data);

#if LIBPWMD_VERSION >= 0x603
	// For verifying a knownhost over an SSH connection.
	typedef gpg_error_t (*KnownhostCallback)(void *data, const char *host,
		const char *key, size_t len);

	// The data parameter is user data passed to the callback.
	gpg_error_t setKnownhostCallback(KnownhostCallback cb, void *data);
#endif

	// Returns the currently set socket path.
	QString socket();
	
	// Will not be considered until the next connect().
	void setSocket(const QString &sock);

	// Returns the currently set data filename.
	QString filename();

	// To be called before open() and when a new data file is to be opened.
	// The default the value passed to the constructor.
	void setFilename(const QString &filename);

	// Sends a 'command' to the pwmd server returning any result and
	// setting 'rc' if there was an error. The 'autoOpen' parameter
	// specifies whether this command should attempt to open the data
	// file. The default is to both connect the socket and open the data
	// file before sending the command.
	virtual QString command(const QString &command, gpg_error_t &rc,
		bool autoOpen = true);

	// For commands that use an INQUIRE.
	virtual gpg_error_t inquire(const char *command, const QString &data);

	// Connect the socket and open the data file when 'autoOpen' is true.
	virtual gpg_error_t connect(bool autoOpen = true);

	// Open the data file set with setFilename(). The 'force' parameter
	// specifies whether or not to attempt to open the file when the
	// connection state is already Opened.
	virtual gpg_error_t open(bool force = true);

	// Saves any changes. A wrapper around pwmd_save()/pwmd_save2().
	virtual gpg_error_t save();

	// Closes any active handle and resets the connection state to Init.
	virtual gpg_error_t reset();

	// These should be set before opening a file.
	void setLockOnOpen(bool = true); // pwmd option LOCK_ON_OPEN
	bool lockOnOpen();
	void setErrorOnLocked(bool = true); // pwmd option RC_ON_LOCKED
	bool errorOnLocked();

	// If you need call other libpwmd functions then you might need this.
	pwm_t *handle();

    signals:
	// Emitted when the connection state changes.
	void stateChanged(Pwmd::ConnectionState);

	// Emitted when a connection error occurs. This won't do anything else
	// but emit the signal. You'll have to reset() or do whatever is
	// needed.
	void connectionError(unsigned rc);

    private:
	static gpg_error_t inquireCallback(void *user, const char *cmd,
		gpg_error_t rc, char **data, size_t *len);
	void resetHandle();

	friend class PwmdStatusMessageThread;
	PwmdStatusMessageThread statusMsgThread;
	QMutex *commandMutex;
	pwm_t *pwm;
	ConnectionState _state;
	QString _socket;
	QString _filename;
	StatusCallback statusCb;
	void *statusCbData;
	QString clientName;
	bool _lockOnOpen;
	bool _errorOnLocked;
#if LIBPWMD_VERSION >= 0x603
	KnownhostCallback knownhostCb;
	void *knownhostCbData;
#endif
};

#endif
