/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef PWMDTREEWIDGET_H
#define PWMDTREEWIDGET_H

#include <QTreeWidget>
#include <QTimer>
#include <QLineEdit>
#include <QItemDelegate>
#include <QRegExp>
#include <QValidator>
#include <QKeyEvent>

class PwmdDialog;
class PwmdTreeWidget : public QTreeWidget
{
    Q_OBJECT

    public:
	PwmdTreeWidget(QWidget *);
	~PwmdTreeWidget() {};
	void setParent(PwmdDialog *);

    private slots:
	void slotExpandTimeout();

    private:
	void keyPressEvent(QKeyEvent *);
	void mouseDoubleClickEvent(QMouseEvent *);
	void mousePressEvent(QMouseEvent *);
	void mouseReleaseEvent(QMouseEvent *);
	void mouseMoveEvent(QMouseEvent *);
	void dragEnterEvent(QDragEnterEvent *);
	void dragMoveEvent(QDragMoveEvent *);
	QStringList mimeTypes() const;
	Qt::DropActions supportedDropActions() const;
	bool dropMimeData(QTreeWidgetItem *, int, const QMimeData *, Qt::DropAction);
	bool isValidDndMove(QTreeWidgetItem *);

	PwmdDialog *parent;
	QPoint dragStartPosition;
	QTimer *expandTimer;
	QTreeWidgetItem *lastItem;
	bool dndMove;
};

class PwmdTreeWidgetItemDelegate : public QItemDelegate
{
    Q_OBJECT

    public:
	PwmdTreeWidgetItemDelegate(QObject* p = 0) : QItemDelegate(p)
	{
	};

	QWidget *createEditor(QWidget* p, const QStyleOptionViewItem &o,
		const QModelIndex& i) const
	{
	    QWidget* editor = QItemDelegate::createEditor(p, o, i);
	    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);

	    if (lineEdit) {
		QRegExp rx("^[^! \t]+[^ \t]*");
		QValidator *v = new QRegExpValidator(rx, lineEdit);

		lineEdit->setValidator(v);
	    }

	    return editor;
	};

    protected:
	bool eventFilter(QObject *obj, QEvent *ev)
	{
	    if (ev->type() == QEvent::KeyPress) {
		QKeyEvent *kev = static_cast<QKeyEvent *>(ev);

		if (kev->key() == Qt::Key_Tab)
		    return true;
	    }

	    return QItemDelegate::eventFilter(obj, ev);
	};
};

class PwmdTreeWidgetItemData
{
    public:
	PwmdTreeWidgetItemData() {};

	PwmdTreeWidgetItemData(bool t, const QString &n, bool b = false)
	{
	    _name = n;
	    _hasTarget = t;
	    _badTarget = b;
	    _target = QString::null;

	    // Default to true for the first selection of the element.
	    _hasPassword = true;
	};

	PwmdTreeWidgetItemData(const PwmdTreeWidgetItemData &other)
	{
	    _name = other._name;
	    _hasTarget = other._hasTarget;
	    _badTarget = other._badTarget;
	    _target = other._target;
	    _hasPassword = other._hasPassword;
	};

	~PwmdTreeWidgetItemData() {};

	bool hasTarget()
	{
	    return _hasTarget;
	};

	QString target()
	{
	    return _target;
	};

	void setTarget(const QString &s)
	{
	    _target = s;
	};

	bool badTarget()
	{
	    return _badTarget;
	};

	void setBadTarget(bool b = true)
	{
	    _badTarget = b;
	};

	QString name()
	{
	    return _name;
	};

	void setHasPassword(bool b)
	{
	    _hasPassword = b;
	};

	bool hasPassword()
	{
	    return _hasPassword;
	};

    private:
	bool _hasTarget;
	bool _badTarget;
	bool _hasPassword;
	QString _name;
	QString _target;
};

Q_DECLARE_METATYPE(PwmdTreeWidgetItemData)

#endif
