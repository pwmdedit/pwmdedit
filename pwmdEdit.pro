PREFIX = /usr/local
BINDIR = /usr/local/bin
LIBDIR = /usr/local/lib
DATADIR = /usr/local/share

TARGET = pwmdEdit
CONFIG += release

*-g++*:QMAKE_CFLAGS += -g -O2
*-g++*:QMAKE_CXXFLAGS += -g -O2

HEADERS += $$PWD/pwmdEdit.h \
	   $$PWD/pwmdShortcutDialog.h
SOURCES += $$PWD/pwmdEdit.cpp \
           $$PWD/main.cpp \
	   $$PWD/pwmdShortcutDialog.cpp
INTERFACES += $$PWD/pwmdShortcutDialog.ui
RESOURCES += pwmdEdit.qrc
EXTRAS += trayicon.svg

include($$PWD/include.pri)

version.target = version
version.depends = $$TARGET
version.commands = $$PWD/makever.sh $$HEADERS $$SOURCES $$INTERFACES \
                   $$RESOURCES pwmdEdit.pro include.pri COPYING README \
		   $$EXTRAS

QMAKE_EXTRA_TARGETS += version
