/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#ifndef PWMDSHORTCUTDIALOG_H
#define PWMDSHORTCUTDIALOG_H

#include "ui_pwmdShortcutDialog.h"

class PwmdShortcutDialog : public QDialog
{
    Q_OBJECT

    public:
	PwmdShortcutDialog(const QString & = 0, const QString & = 0);
	~PwmdShortcutDialog();

    private slots:
	void slotNewShortcut();
	void slotRemoveShortcut();
	void slotShortcutItemChanged(QListWidgetItem *, QListWidgetItem *);
	void slotShortcutNameChanged(const QString &);
	void slotShortcutPathChanged(const QString &);
	void slotChooseElement();

    private:
	Ui::PwmdShortcutDialog ui;
	QString socket;
	QString filename;
};

class PwmdShortcut
{
    public:
	PwmdShortcut() {};

	PwmdShortcut(const QString &n, const QString &p)
	{
	    _name = n;
	    _path = p;
	};

	~PwmdShortcut() {};

	QString name()
	{
	    return _name;
	};

	void setName(const QString &n)
	{
	    _name = n;
	};

	QString path()
	{
	    return _path;
	};

	void setPath(const QString &p)
	{
	    _path = p;
	};

    private:
	QString _name;
	QString _path;
};

Q_DECLARE_METATYPE(PwmdShortcut)

#endif
