This is a libpwmd client using the Qt toolkit to edit a pwmd data file. It's
like an XML editor but handles the "target" attribute.


Requirements:
-------------
libpwmd 6.0 or later and pwmd 2.12 or later. Make sure that
"disable_list_and_dump" is false in your pwmd configuration file (the
default). You'll also need Qt 4.4 or later.


Building:
---------
qmake; make


Usage:
------
Just run ./pwmdEdit after building and making sure pwmd is running. If your
using a non-standard pwmd socket location you can specify it on the command
line with the -s switch or in the Options dialog. A datafile name to open can
also be specified on the command line (no switches needed for this). Most
editing commands are done by using the right mouse button in the element tree
view (or shortcut keys). It's pretty straight forward; it's like a file
manager/xml editor.

There really isn't a standard for applications to follow when it comes to
element paths and element attributes but there is one element attribute that
pwmdEdit treats specially and that is the "password" attribute. This tells
pwmdEdit that the current elements contents contains sensitive data and will
be hidden. Note that this currently is only a single line and not a full text
area. I hope to fix this in a future version.


Ben Kibbey <bjk AT luxsci DOT net>
Jabber: bjk AT thiessen DOT org - (bjk) FreeNode/OFTC
http://bjk.sourceforge.net/pwmd/
