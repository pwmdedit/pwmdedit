#!/bin/sh
set -e
ROOT="pwmdedit-$(sed -ne '/VERSION/{s/"//g;p;q}' < pwmdDialog.h | awk '{print $3}')"
TGZ="$ROOT.tar.gz"

mkdir -p $ROOT
git log > $ROOT/ChangeLog

for f in $@; do
	cp $f "$ROOT"
done

tar zcf "$TGZ" "$ROOT"
