/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <QApplication>
#include <QClipboard>
#include <QDateTime>
#include "pwmdGenPassDialog.h"

PwmdGenPassDialog::PwmdGenPassDialog(QWidget *p) : QDialog(p)
{
    _result = QString::null;
    ui.setupUi(this);
    connect(ui.ck_showPassword, SIGNAL(clicked()), this, SLOT(slotShowPassword()));
    connect(ui.tb_generate, SIGNAL(clicked()), this, SLOT(slotGeneratePassword()));
    connect(ui.generateComboBox, SIGNAL(activated(int)), this, SLOT(slotGenerateTypeChanged(int)));
    connect(ui.tb_clipboard, SIGNAL(clicked()), this, SLOT(slotClipboardPassword()));
}

QString PwmdGenPassDialog::generatePassword(QWidget *p)
{
    PwmdGenPassDialog d(p);

    if (d.exec() == QDialog::Accepted)
	return d.result();

    return QString();
}

void PwmdGenPassDialog::slotShowPassword()
{
    bool b = ui.ck_showPassword->isChecked();

    ui.le_password->setEchoMode(!b ? QLineEdit::Password : QLineEdit::Normal);
}

void PwmdGenPassDialog::slotGeneratePassword()
{
    _result = QString::null;

    switch (ui.generateComboBox->currentIndex()) {
	case 0:
	    _result = PwmdGenPassDialog::generate(PwmdGenPassDialog::Any, ui.lengthSpinBox->value());
	    break;
	case 1:
	    _result = PwmdGenPassDialog::generate(PwmdGenPassDialog::AlphaNumeric, ui.lengthSpinBox->value());
	    break;
	case 2:
	    _result = PwmdGenPassDialog::generate(PwmdGenPassDialog::Alpha, ui.lengthSpinBox->value());
	    break;
	case 3:
	    _result = PwmdGenPassDialog::generate(PwmdGenPassDialog::Numeric, ui.lengthSpinBox->value());
	    break;
	case 4:
	    if (ui.le_only->text().isEmpty())
		break;

	    _result = PwmdGenPassDialog::generate(ui.le_only->text(), ui.lengthSpinBox->value());
	    break;
	default:
	    break;
    }

    ui.le_password->setText(_result);
}

void PwmdGenPassDialog::slotGenerateTypeChanged(int idx)
{
    bool b = false;

    if (idx == ui.generateComboBox->count()-1)
	b = true;

    ui.le_only->setEnabled(b);
}

QString PwmdGenPassDialog::generate(const QString &str, int len)
{
    QString s = QString::null;
    static bool init = false;

    if (!init) {
	qsrand(QDateTime::currentDateTime().toTime_t() ^ (getpid() + (getpid() << 15)));
	init = true;
    }

    for (int i = 0; i < len; i++)
	s.append(str.at(qrand() % str.length()));

    return s;
}

QString PwmdGenPassDialog::generate(Type t, int len)
{
    QString result = QString::null;
    bool done = true;

    do {
	switch (t) {
	    case Any:
		result = generate("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`~-_=+!@#$%^&*()][\\{}|';\":?></.,", len);
		break;
	    case AlphaNumeric:
		result = generate("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", len);

		if (!result.contains(QRegExp("[0-9]+")) ||
			!result.contains(QRegExp("[a-z]+", Qt::CaseInsensitive)))
		    done = false;
		else
		    done = true;

		break;
	    case Alpha:
		result = generate("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", len);
		break;
	    case Numeric:
		result = generate("0123456789", len);
		break;
	    default:
		break;
	}
    } while (!done);

    return result;
}

QString PwmdGenPassDialog::result()
{
    return _result;
}

void PwmdGenPassDialog::slotClipboardPassword()
{
    QClipboard *c = QApplication::clipboard();

    c->setText(ui.le_password->text(), QClipboard::Selection);
    c->setText(ui.le_password->text());
}
