/* vim:tw=78:ts=8:sw=4:set ft=cpp:  */
/*
    Copyright (C) 2010 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02110-1301  USA
*/
#include <QApplication>
#include <QMenu>
#include <QSettings>
#include <QClipboard>
#include <QTimer>
#include "pwmdEdit.h"

PwmdEdit::PwmdEdit(int argc, char **argv) : QApplication(argc, argv)
{
    filename = QString::null;
    socket = QString::null;
    QStringList args = arguments();
    bool editOnly = false;

    for (int i = 1; i < args.count(); i++) {
	QString arg = args.at(i);

	if (arg.at(0) == '-') {
	    switch (arg.at(1).toLatin1()) {
		case 's':
		    socket = args.at(++i);
		    break;
		case 'e':
		    editOnly = true;
		    break;
	    }
	}
	else
	    filename = arg;
    }

    if (!editOnly) {
	clipboardTimer = new QTimer(this);
	connect(clipboardTimer, SIGNAL(timeout()), SLOT(slotClearClipboard()));
	setQuitOnLastWindowClosed(false);
	shortcutMenu = new QMenu();
	connect(shortcutMenu, SIGNAL(triggered(QAction *)), this, SLOT(slotShortCut(QAction *)));
	refreshShortcuts();
	sti = new QSystemTrayIcon(QIcon(":trayicon.svg"), this);
	connect(sti, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), SLOT(slotSystemTrayActivated(QSystemTrayIcon::ActivationReason)));
	sti->setContextMenu(shortcutMenu);
	sti->show();
    }
    else {
	slotEditPwmd();
	QTimer::singleShot(1, this, SLOT(quit()));
    }
}

void PwmdEdit::refreshShortcuts()
{
    shortcutMenu->clear();
    shortcutMenu->addAction(tr("Edit XML"), this, SLOT(slotEditPwmd()));
    shortcutMenu->addAction(tr("Edit shortcuts"), this, SLOT(slotEditShortcuts()));
    shortcutMenu->addSeparator();
    QSettings cfg("pwmdDialog");

    if (filename.isEmpty())
	filename = cfg.value("lastOpenedFile", "").toString();

    clipboardTimeout = cfg.value("clipboardTimeout", 20).toInt();
    int size = cfg.beginReadArray("shortcuts");

    for (int i = 0; i < size; ++i) {
	cfg.setArrayIndex(i);
	PwmdShortcut data = PwmdShortcut(cfg.value("name").toString(),
		cfg.value("path").toString());

	QAction *a = shortcutMenu->addAction(data.name());
	a->setData(QVariant::fromValue(data));
    }

    cfg.endArray();

    if (size)
	shortcutMenu->addSeparator();

    shortcutMenu->addAction(tr("Clear clipboard"), this, SLOT(slotClearClipboard()));
    shortcutMenu->addAction(tr("Quit"), this, SLOT(quit()));
}

void PwmdEdit::slotClearClipboard()
{
    PwmdDialog::clearClipboard();
}

void PwmdEdit::slotEditShortcuts()
{
    PwmdShortcutDialog *d = new PwmdShortcutDialog();
    bool b = d->exec();

    delete d;

    if (b)
	refreshShortcuts();
}

void PwmdEdit::slotShortCut(QAction *a)
{
    PwmdShortcut data = a->data().value<PwmdShortcut>();
    gpg_error_t rc;

    if (data.name().isEmpty())
	return;

    Pwmd *pwm = new Pwmd(socket, filename, "pwmdEdit");
    QString s = QString("GET %1").arg(data.path());
    QString result = pwm->command(s, rc);

    delete pwm;

    if (rc) {
	sti->showMessage(tr("There was an error while communicating with pwmd."),
	QString("ERR %1: %2").arg(rc).arg(pwmd_strerror(rc)));
    }
    else {
	QClipboard *c = QApplication::clipboard();

	c->setText(result, QClipboard::Selection);
	c->setText(result);
	refreshShortcuts();

	if (clipboardTimeout)
	    clipboardTimer->start(clipboardTimeout*1000);
    }
}

void PwmdEdit::slotSystemTrayActivated(QSystemTrayIcon::ActivationReason n)
{
    if (n == QSystemTrayIcon::DoubleClick)
	slotEditPwmd();
}

void PwmdEdit::slotEditPwmd()
{
    PwmdDialog *d = new PwmdDialog(socket, filename, "pwmdEdit");

    d->exec();
    delete d;
}
